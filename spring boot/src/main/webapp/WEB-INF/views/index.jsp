<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1" isELIgnored="false"%> <%@ taglib prefix="c"
uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <button onclick="send()">Send Data</button>
    <button onClick="window.location.href=window.location.href">show</button>
    <h1>List of Model</h1>
    <table border="1">
      <tr>
        <th>ID</th>
        <th>PostId</th>
        <th>Name</th>
        <th>Email</th>
        <th>Body</th>
      </tr>
      <c:forEach items="${list}" var="e">
        <tr>
          <td>${e.getId()}</td>
          <td>${e.getPostId()}</td>
          <td>${e.getName()}</td>
          <td>${e.getEmail()}</td>
          <td>${e.getBody()}</td>
        </tr>
      </c:forEach>
    </table>

    <script>
      function send() {
        console.log(123);
        let fetchRes = fetch(
          "https://jsonplaceholder.typicode.com/posts/1/comments"
        );
        fetchRes
          .then((res) => res.json())
          .then((d) => {
            console.log(typeof(d))
            let options = {
              method: "POST",
              headers: {
                "Content-Type": "application/json;charset=utf-8",
              },
              body: JSON.stringify(d),
            };
            
            //   // api for making post requests
            let fetchs = fetch("http://localhost:8080/save", options);
          });
      }
    </script>
  </body>
</html>
