package com.nexsoft.app.repo;

import java.util.ArrayList;

import com.nexsoft.app.model.Services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Servicerepository extends JpaRepository<Services, Integer> {
	@Query(nativeQuery = true, value = "select * from services e where e.deleted=false")
	ArrayList<Services> find();

	@Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update services s set deleted=true where s.id= :id")
	void delete(@Param("id") Integer id);

	Services findTopByOrderByIdDesc();
}
