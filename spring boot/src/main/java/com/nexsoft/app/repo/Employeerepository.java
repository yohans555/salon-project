package com.nexsoft.app.repo;

import java.util.ArrayList;

import com.nexsoft.app.model.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Employeerepository extends JpaRepository<Employee, Integer> {
	@Query(nativeQuery = true, value = "select * from employee e where e.deleted=false")
	ArrayList<Employee> find();

	@Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update employee e set deleted=true where e.id= :id")
	void delete(@Param("id") Integer id);

	Employee findTopByOrderByIdDesc();
}
