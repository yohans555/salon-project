package com.nexsoft.app.repo;

import java.util.ArrayList;

import com.nexsoft.app.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Productrepository extends JpaRepository<Product, Integer> {
	@Query(nativeQuery = true, value = "select * from product e where e.deleted=false")
	ArrayList<Product> find();

	@Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update product p set deleted=true where p.id= :id")
	void delete(@Param("id") Integer id);

	Product findTopByOrderByIdDesc();
}
