package com.nexsoft.app.repo;

import com.nexsoft.app.model.ServiceCategory;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceCategoryrepository extends JpaRepository<ServiceCategory, Integer> {

}
