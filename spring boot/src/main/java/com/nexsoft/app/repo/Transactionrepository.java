package com.nexsoft.app.repo;

import java.util.ArrayList;
import java.util.List;

import com.nexsoft.app.model.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Transactionrepository extends JpaRepository<Transaction, Integer> {
	@Query(nativeQuery = true, value = "select * from  transaction t where t.date =curdate()")
	public List<Transaction> findTodayTransactions();

	@Query(nativeQuery = true, value = "select * from transaction e where e.deleted=false")
	ArrayList<Transaction> find();

	@Modifying(clearAutomatically = true)
	@Query(nativeQuery = true, value = "update transaction t set deleted=true where t.id= :id")
	void delete(@Param("id") Integer id);

	public List<Transaction> findByUsername(String username);

	Transaction findTopByOrderByIdDesc();
}
