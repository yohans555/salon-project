package com.nexsoft.app.model;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String productCode;
	private String brand;
	private String name;
	private int price;
	private String picture;
	private boolean deleted = false;

	@ManyToOne
	@JoinColumn(name = "service_category_id")
	private ServiceCategory serviceCategory;

	public Product() {
	}

	public Product(int id, String productCode, String brand, String name, int price, String picture,
			ServiceCategory serviceCategory, boolean deleted) {
		super();
		this.id = id;
		this.productCode = productCode;
		this.brand = brand;
		this.name = name;
		this.price = price;
		this.picture = picture;
		this.serviceCategory = serviceCategory;
		this.deleted = deleted;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public ServiceCategory getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(ServiceCategory serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

}
