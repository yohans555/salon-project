package com.nexsoft.app.model;

import javax.persistence.*;

@Entity
@Table(name = "services")
public class Services {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;
	private int price;
	private boolean deleted;

	@ManyToOne
	@JoinColumn(name = "service_category_id")
	private ServiceCategory serviceCategory;

	public Services() {
	}

	public Services(int id, String name, int price, ServiceCategory serviceCategory, boolean deleted) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.serviceCategory = serviceCategory;
		this.deleted = deleted;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public ServiceCategory getServiceCategory() {
		return serviceCategory;
	}

	public void setServiceCategory(ServiceCategory serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

}
