package com.nexsoft.app.model;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class EmployeeDTO {
	private int id;
	private String firstName;
	private String lastName;
	private String birthdate;
	private String idCard;
	private int phone;
	private String email;
	private String address;
	private List<MultipartFile> profilePicture;
	private boolean hairCut;
	private boolean coloring;
	private boolean creamBath;
	private boolean waxing;
	private boolean grooming;
	private boolean styling;
	private String gender;
	private boolean deleted = false;

	public EmployeeDTO() {
	}

	public EmployeeDTO(int id, String firstName, String lastName, String birthdate, String idCard, int phone,
			String email, String address, List<MultipartFile> profilePicture, boolean hairCut, boolean coloring,
			boolean creamBath, boolean waxing, boolean grooming, boolean styling, String gender, Boolean deleted) {
		super();
		this.id = id;
		this.gender = gender;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.idCard = idCard;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.profilePicture = profilePicture;
		this.hairCut = hairCut;
		this.coloring = coloring;
		this.creamBath = creamBath;
		this.waxing = waxing;
		this.grooming = grooming;
		this.styling = styling;
		this.deleted = deleted;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<MultipartFile> getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(List<MultipartFile> profilePicture) {
		this.profilePicture = profilePicture;
	}

	public boolean isHairCut() {
		return hairCut;
	}

	public void setHairCut(boolean hairCut) {
		this.hairCut = hairCut;
	}

	public boolean isColoring() {
		return coloring;
	}

	public void setColoring(boolean coloring) {
		this.coloring = coloring;
	}

	public boolean isCreamBath() {
		return creamBath;
	}

	public void setCreamBath(boolean creamBath) {
		this.creamBath = creamBath;
	}

	public boolean isWaxing() {
		return waxing;
	}

	public void setWaxing(boolean waxing) {
		this.waxing = waxing;
	}

	public boolean isGrooming() {
		return grooming;
	}

	public void setGrooming(boolean grooming) {
		this.grooming = grooming;
	}

	public boolean isStyling() {
		return styling;
	}

	public void setStyling(boolean styling) {
		this.styling = styling;
	}

}
