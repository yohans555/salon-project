package com.nexsoft.app.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.nexsoft.app.model.*;
import com.nexsoft.app.repo.Servicerepository;
import com.nexsoft.app.service.Modelservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class GetController {

	@Autowired
	Modelservice modelservice;

	@Autowired
	Servicerepository servicerepository;

	@GetMapping(value = "/get/transaction", produces = "application/json")
	public ArrayList<Transaction> getTransactions() {
		return modelservice.getTransaction();
	}

	@GetMapping(value = "/get/todaytransaction", produces = "application/json")
	public ArrayList<Transaction> getTodayTransactions() {
		return modelservice.getTodayTransaction();
	}

	@GetMapping(value = "/get/product", produces = "application/json")
	public ArrayList<Product> getProducts() {
		return modelservice.getProducts();
	}

	@GetMapping(value = "/get/service", produces = "application/json")
	public ArrayList<Services> getService() {
		return modelservice.getService();
	}

	@GetMapping(value = "/get/service_category", produces = "application/json")
	public ArrayList<ServiceCategory> getServiceCategories() {
		return modelservice.getServiceCategory();
	}

	@GetMapping(value = "/get/service/{id}", produces = "application/json")
	public Optional<Services> getServiceById(@PathVariable(value = "id") int id) {
		return servicerepository.findById(id);
	}

	@GetMapping(value = "/get/employee", produces = "application/json")
	public ArrayList<Employee> getEmployee() {
		return modelservice.getEmployee();
	}

	@GetMapping(value = "/get/category", produces = "application/json")
	public ArrayList<ServiceCategory> getCategory() {
		return modelservice.getServiceCategory();
	}

	// Detail Controller
	@GetMapping(value = "/detail/employee/{id}", produces = "application/json")
	public Employee detailEmployee(@PathVariable("id") int id) {
		Optional<Employee> list = modelservice.getEmployeeById(id);
		Employee employee = list.get();
		return employee;
	}

	@GetMapping(value = "/detail/product/{id}", produces = "application/json")
	public Product detailProduct(@PathVariable("id") int id) {
		Optional<Product> list = modelservice.getProductById(id);
		Product product = list.get();
		return product;
	}

	@GetMapping(value = "/detail/transaction/{id}", produces = "application/json")
	public Transaction detailTransaction(@PathVariable("id") int id) {
		Optional<Transaction> list = modelservice.getTransactionId(id);
		Transaction transaction = list.get();
		return transaction;
	}

	@GetMapping(value = "/detail/services/{id}", produces = "application/json")
	public Services detail(@PathVariable("id") int id) {
		Optional<Services> list = modelservice.getServiceById(id);
		Services services = list.get();
		return services;
	}

	@GetMapping(value = "get/detail/transaction/{name}", produces = "application/json")
	public List<Transaction> getTransactionByName(@PathVariable("name") String name) {
		return modelservice.findbyusername(name);
	}
}
