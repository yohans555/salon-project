package com.nexsoft.app.Controller;

import com.nexsoft.app.service.Modelservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class DeleteController {

	@Autowired
	Modelservice modelservice;

	@DeleteMapping("/delete/product/{id}")
	public void deleteProduct(@PathVariable("id") int id) {
		modelservice.deleteProduct(id);
	}

	@DeleteMapping("/delete/employee/{id}")
	public void deleteEmployee(@PathVariable("id") int id) {
		modelservice.deleteEmployee(id);
	}

	@DeleteMapping("/delete/services/{id}")
	public void deleteService(@PathVariable("id") int id) {
		modelservice.deleteService(id);
	}

}
