package com.nexsoft.app.Controller;

import java.io.IOException;

import com.nexsoft.app.model.*;
import com.nexsoft.app.service.MailService;
import com.nexsoft.app.service.Modelservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class SaveController {

	@Autowired
	Modelservice modelservice;

	@Autowired
	MailService mailService;

	@PostMapping(value = "/save/product", consumes = "multipart/form-data")
	public void add(@ModelAttribute ProductDTO productDTO, RedirectAttributes redirectAttributes) {
		try {
			modelservice.saveProduct(productDTO);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@PostMapping(value = "save/emp", consumes = "application/json")
	public void add(@RequestBody Employee employee) {
		modelservice.saveEmp(employee);
	}

	@PostMapping(value = "save/services", consumes = "application/json")
	public void add(@RequestBody Services services) {
		modelservice.saveService(services);
	}

	@PostMapping(value = "/save/transaction", consumes = "application/json")
	public void add(@RequestBody Transaction transaction) {
		modelservice.saveTransaction(transaction);
		mailService.sendEmail();
	}

	@PostMapping(value = "/save/employee", consumes = "multipart/form-data")
	public void add(@ModelAttribute EmployeeDTO employeeDTO, RedirectAttributes redirectAttributes) {
		try {
			modelservice.saveEmployee(employeeDTO);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
