package com.nexsoft.app.service;

import javax.transaction.Transactional;

import com.nexsoft.app.model.Services;
import com.nexsoft.app.model.Transaction;
import com.nexsoft.app.repo.Servicerepository;
import com.nexsoft.app.repo.Transactionrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * MailService
 */
@Service
public class MailService {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	Transactionrepository transactionrepository;

	@Autowired
	Servicerepository servicerepository;

	@Transactional
	public void sendEmail() {
		Transaction transaction = transactionrepository.findTopByOrderByIdDesc();
		String content = "Terimakasih atas kunjungan anda berikut terlampir invoice anda \n";
		System.out.println(transaction.getEmployee().getId() + transaction.getEmployee().getEmail());
		for (Services services : transaction.getServices()) {
			Services temp = servicerepository.findById(services.getId()).get();
			content = content + temp.getName() + ":" + temp.getPrice() + "\n";
		}
		content = content + "\n total:" + transaction.getTotal();
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(transaction.getEmail());
		msg.setSubject("InVoice");
		msg.setText(content);
		javaMailSender.send(msg);

	}
}