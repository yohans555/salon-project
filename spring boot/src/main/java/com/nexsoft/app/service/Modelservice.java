package com.nexsoft.app.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.nexsoft.app.model.*;
import com.nexsoft.app.repo.*;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class Modelservice {
<<<<<<< HEAD
	@Autowired
	private Employeerepository employeerepository;
	@Autowired
	private Transactionrepository transactionrepository;
	@Autowired
	private Productrepository productrepository;
	@Autowired
	private Servicerepository servicerepository;
	@Autowired
	private ServiceCategoryrepository serviceCategoryrepository;

	// Get All Controller
	@Transactional
	public ArrayList<Employee> getEmployee() {
		return (ArrayList<Employee>) employeerepository.find();
	}

	@Transactional
	public ArrayList<Product> getProducts() {
		return (ArrayList<Product>) productrepository.find();
	}

	@Transactional
	public ArrayList<Transaction> getTransaction() {
		return (ArrayList<Transaction>) transactionrepository.find();
	}

	@Transactional
	public ArrayList<Transaction> getTodayTransaction() {
		return (ArrayList<Transaction>) transactionrepository.findTodayTransactions();
	}

	@Transactional
	public ArrayList<Services> getService() {
		return (ArrayList<Services>) servicerepository.find();
	}

	@Transactional
	public ArrayList<ServiceCategory> getServiceCategory() {
		return (ArrayList<ServiceCategory>) serviceCategoryrepository.findAll();
	}

	@Transactional
	public void saveEmployee(EmployeeDTO employeeDTO) throws IOException {
		ModelMapper mapper = new ModelMapper();
		Employee emp = mapper.map(employeeDTO, Employee.class);
		List<MultipartFile> image = employeeDTO.getProfilePicture();
		String listimage = "";
		for (MultipartFile file : image) {
			byte[] bytes = file.getBytes();
			Path path = Paths
					.get("D:\\Project Akhir\\salon-project\\React\\salon-app\\public" + file.getOriginalFilename());
			Files.write(path, bytes);
			listimage = listimage + "/" + file.getOriginalFilename();
		}
		emp.setProfilePicture(listimage);
		employeerepository.save(emp);
	}

	@Transactional
	public void saveProduct(ProductDTO productDTO) throws IOException {
		ModelMapper mapper = new ModelMapper();
		Product product = mapper.map(productDTO, Product.class);
		String listimage = "";
		List<MultipartFile> image = productDTO.getPicture();
		for (MultipartFile file : image) {
			byte[] bytes = file.getBytes();
			Path path = Paths.get("C:\\Users\\NEXSOFT\\Desktop\\salon-project\\React\\salon-app\\public\\"
					+ file.getOriginalFilename());
			Files.write(path, bytes);
			listimage = listimage + "/" + file.getOriginalFilename() + ";";
		}
		product.setPicture(listimage);
		productrepository.save(product);
	}

	@Transactional
	public void saveEmp(Employee employee) {
		employeerepository.save(employee);
	}

	@Transactional
	public void saveService(Services service) {
		servicerepository.save(service);
	}

	@Transactional
	public void saveServiceCategory(ServiceCategory serviceCategory) {
		serviceCategoryrepository.save(serviceCategory);
	}

	@Transactional
	public void saveTransaction(Transaction transaction) {
		transactionrepository.save(transaction);
	}

	// Delete controller
	@Transactional
	public void deleteProduct(Integer id) {
		productrepository.delete(id);
	}

	@Transactional
	public void deleteService(Integer id) {
		servicerepository.delete(id);
	}

	@Transactional
	public void deleteEmployee(Integer id) {
		employeerepository.delete(id);
	}

	// Get by Id controller
	@Transactional
	public Optional<Employee> getEmployeeById(Integer id) {
		return employeerepository.findById(id);
	}

	@Transactional
	public Optional<Transaction> getTransactionId(Integer id) {
		return transactionrepository.findById(id);
	}

	@Transactional
	public Optional<Product> getProductById(Integer id) {
		return productrepository.findById(id);
	}

	@Transactional
	public Optional<Services> getServiceById(Integer id) {
		return servicerepository.findById(id);
	}

	@Transactional
	public List<Transaction> findbyusername(String username) {
		return transactionrepository.findByUsername(username);
	}
=======
     @Autowired
     private Employeerepository employeerepository;
     @Autowired
     private Transactionrepository transactionrepository;
     @Autowired
     private Productrepository productrepository;
     @Autowired
     private Servicerepository servicerepository;
     @Autowired
     private ServiceCategoryrepository serviceCategoryrepository;


    // Get All Controller
    @Transactional
    public ArrayList<Employee> getEmployee(){
        return (ArrayList<Employee>) employeerepository.find();
    }

    @Transactional
    public ArrayList<Product> getProducts(){
        return (ArrayList<Product>) productrepository.find();
    }

   @Transactional
   public ArrayList<Transaction> getTransaction(){
       return (ArrayList<Transaction>) transactionrepository.find();
   }

   @Transactional
   public ArrayList<Transaction> getTodayTransaction(){
       return (ArrayList<Transaction>) transactionrepository.findTodayTransactions();
   }
    
    @Transactional
    public ArrayList<Services> getService(){
        return  (ArrayList<Services>) servicerepository.find();
    }

    @Transactional
    public ArrayList<ServiceCategory> getServiceCategory(){
        return (ArrayList<ServiceCategory>) serviceCategoryrepository.findAll();
    }

    @Transactional
    public void saveEmployee(EmployeeDTO employeeDTO) throws IOException{
        ModelMapper mapper = new ModelMapper();
        Employee emp = mapper.map(employeeDTO, Employee.class);
        List<MultipartFile> image = employeeDTO.getProfilePicture();
        String listimage ="";
        for (MultipartFile file : image) {
                byte[] bytes = file.getBytes();
                Path path = Paths.get("D:\\Project Akhir\\salon-project\\React\\salon-app\\public" + file.getOriginalFilename());
                Files.write(path, bytes);
                listimage = listimage +"/"+file.getOriginalFilename();
        }
        emp.setProfilePicture(listimage);
        employeerepository.save(emp);
    }

    @Transactional
    public void saveProduct(ProductDTO productDTO) throws IOException{
        ModelMapper mapper = new ModelMapper();
        Product product = mapper.map(productDTO, Product.class);
        String listimage ="";
            List<MultipartFile> image = productDTO.getPicture();
            for (MultipartFile file : image) {
                byte[] bytes = file.getBytes();
                Path path = Paths.get("D:\\Project Akhir\\salon-project\\React\\salon-app\\public" + file.getOriginalFilename());
                Files.write(path, bytes);
                listimage = listimage +"/"+file.getOriginalFilename();
            }
        product.setPicture(listimage);
        productrepository.save(product);
    }

    @Transactional
    public void saveEmp(Employee employee){
        employeerepository.save(employee);
    }

    @Transactional
    public void saveService(Services service){
        servicerepository.save(service);
    }
    
    @Transactional
    public void saveServiceCategory(ServiceCategory serviceCategory){
        serviceCategoryrepository.save(serviceCategory);
    }

   @Transactional
   public void saveTransaction(Transaction transaction){
       transactionrepository.save(transaction);
   }

    //Delete controller
    @Transactional
    public void deleteProduct(Integer id){
        productrepository.delete(id);
    }

    @Transactional
    public void deleteService(Integer id){
        servicerepository.delete(id);
    }

    @Transactional
    public void deleteEmployee(Integer id){
        employeerepository.delete(id);
    }


    //Get by Id controller
    @Transactional
    public Optional<Employee> getEmployeeById(Integer id){
        return employeerepository.findById(id);
    }

   @Transactional
   public Optional<Transaction> getTransactionId(Integer id){
       return transactionrepository.findById(id);
   }

    @Transactional
    public Optional<Product> getProductById(Integer id){
        return productrepository.findById(id);
    }

    @Transactional
    public Optional<Services> getServiceById(Integer id){
        return servicerepository.findById(id);
    }

    @Transactional
    public List<Transaction> findbyusername(String username){
        return transactionrepository.findByUsername(username);
    }

    
>>>>>>> e448a05eb9fa64a9df09e98bc6821bf443bab9a5
}
