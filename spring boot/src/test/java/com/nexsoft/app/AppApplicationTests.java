package com.nexsoft.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Date;

import com.nexsoft.app.model.*;
import com.nexsoft.app.repo.*;
import com.nexsoft.app.service.Modelservice;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
class AppApplicationTests {

	@Autowired
	Modelservice modelservice;
	@Autowired
      Employeerepository employeerepository;
     @Autowired
      Transactionrepository transactionrepository;
     @Autowired
      Productrepository productrepository;
     @Autowired
      Servicerepository servicerepository;
     @Autowired
      ServiceCategoryrepository serviceCategoryrepository;

	  Date date = new Date();
	  Employee employee =new Employee(123, "firstName", "lastName", "birthdate", "idCard", "gender", 123, "email", "addres", "profilePicture", true, true, true, true, true, true,false);
	  ServiceCategory serviceCategory = new ServiceCategory(1,"waxing");
	  Services services = new Services(999,"unitest",999,serviceCategory,false);



	@Test
	void PostGetService() {
		
		modelservice.saveService(services);
		Services actual = servicerepository.findTopByOrderByIdDesc();
		System.out.println(actual.getName());
		assertEquals(services.getName(), actual.getName(), "Membandingkan method post dan get untuk model product");
	}

	@Test()
	void deleteService() {
		Services data = servicerepository.findTopByOrderByIdDesc();
		int id = data.getId();
		modelservice.deleteService(id);
		assertFalse(!modelservice.getServiceById(id).get().isDeleted());
	}

	@Test
	void findServices() {
		assertTrue(modelservice.getServiceById(11).isPresent());
	}

	@Test
	void TransactionDate() {
		ArrayList<Transaction> actual=modelservice.getTodayTransaction();
		int error=0;
		Date curDate = new Date();
		for (Transaction transaction : actual) {
			if (transaction.getDate()!=curDate) {
				error = error+1;
			}
		}
		assertEquals(0, error);
	}

	@Test
	void PostGetTransaction() {
		Transaction transaction = new Transaction() ;
		transaction.setDeleted(false);
		transaction.setPhone(124);
		modelservice.saveTransaction(transaction);
		Transaction actual = transactionrepository.findTopByOrderByIdDesc();
		assertEquals(transaction.getPhone(), actual.getPhone(), "Membandingkan method post dan get untuk model transaction");
	}

	@Test
	void findTransaction() {
		assertTrue(modelservice.getServiceById(11).isPresent());
	}

	@Test
	void findEmployeeTest() {
		assertTrue(modelservice.getEmployeeById(1).get().getGender().equalsIgnoreCase("male"));
	}

	@Test
	void CreatdeleteEmployeeTest() {
		assertTrue(modelservice.getEmployeeById(1).get().getGender().equalsIgnoreCase("male"));
	}

	
}
