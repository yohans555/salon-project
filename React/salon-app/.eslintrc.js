module.exports = {
  parser: "babel-eslint",
  root: true,
  extends: '@react-native-community',
  rules: {
    'prettier/prettier': 0,
    "react/prop-types": "off",
    "no-unused-vars":"off"
  },
  env: {
    es6: true,
    node: true,
    browser: true,
  },
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ["react"],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
  ],
};
