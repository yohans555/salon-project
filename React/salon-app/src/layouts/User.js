import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import UserNavbar from "../userComponents/Navbar/UserNavbar";
import routesUser from "./routesUser";

const switchRoutes = (
  <Switch>
    {routesUser.map((prop, key) => {
      return (
        <Route
          path={prop.layout + prop.path}
          component={prop.component}
          key={key}
        />
      );
    })}
  </Switch>
);

export default function User() {
  return (
    <div>
      <UserNavbar />
      <div>{switchRoutes}</div>
    </div>
  );
}
