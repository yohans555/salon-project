import Profile from "pages/UserPage/Profile";
import Booking from "pages/UserPage/Booking";
import HistoryTransaction from "pages/UserPage/HistoryTransaction";

const routesUser = [
    {
        path: "/profile",
        name: "My Profile",
        component: Profile,
        layout: "/user",
    },
    {
        path: "/booking",
        name: "Book Now",
        component: Booking,
        layout: "/user",
    },
    {
        path: "/history",
        name: "Transaction Details",
        component: HistoryTransaction,
        layout: "/user",
    },
];

export default routesUser;
