import Dashboard from "@material-ui/icons/Dashboard";
import AddBoxIcon from "@material-ui/icons/AddBox";
import Notifications from "@material-ui/icons/Notifications";
import Book from "@material-ui/icons/Book";
import StorefrontIcon from "@material-ui/icons/Storefront";
import BuildIcon from "@material-ui/icons/Build";
import PersonIcon from "@material-ui/icons/Person";
import MenuBookIcon from "@material-ui/icons/MenuBook";
import DashboardPage from "views/Dashboard/Dashboard.js";
import BookingList from "views/BookingList/BookingList.js";
import NewBooking from "views/NewBooking/NewBooking.js";
import App from "../views/Chat/Chat";
import Employee from "views/Employee/Employee.js";
import Service from "views/Services/Service.js";
import Products from "views/Products/Products.js";
import TableHistory from "components/Table/TableHistoryList";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin",
  },
  {
    path: "/booking",
    name: "New Booking",
    icon: AddBoxIcon,
    component: NewBooking,
    layout: "/admin",
  },
  {
    path: "/list",
    name: "Booking List",
    icon: Book,
    component: BookingList,
    layout: "/admin",
  },
  {
    path: "/history",
    name: "Booking History",
    icon: MenuBookIcon,
    component: TableHistory,
    layout: "/admin",
  },
  {
    path: "/employee",
    name: "Employee",
    icon: PersonIcon,
    component: Employee,
    layout: "/admin",
  },
  {
    path: "/services",
    name: "Services",
    icon: BuildIcon,
    component: Service,
    layout: "/admin",
  },
  {
    path: "/products",
    name: "Products",
    icon: StorefrontIcon,
    component: Products,
    layout: "/admin",
  },
  {
    path: "/chat",
    name: "Chat",
    icon: Notifications,
    component: App,
    layout: "/admin",
  },
];

export default dashboardRoutes;
