import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Redirect } from "react-router-dom";
import App from "./App";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <App />
      <Redirect path="/" to="/login" />
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);
