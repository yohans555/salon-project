import React, { useState } from "react";
import { useHistory, Link } from "react-router-dom";
import "../userComponents/signin-register/SignUp.css";
import firebase from "../Firebase.js";
import Footer from "../userComponents/Footer";
import Navbar from "../userComponents/Navbar/Navbar";

function Login() {
  const history = useHistory();
  const [creds, setCreds] = useState({
    nickname: "",
    role: "user",
    password: "",
  });
  const [showLoading, setShowLoading] = useState(false);
  const ref = firebase.database().ref("users/");

  const login = (e) => {
    e.preventDefault();
    setShowLoading(true);
    let a;
    ref
      .orderByChild("nickname")
      .equalTo(creds.nickname)
      .once("value", (snapshot) => {
        if (snapshot.exists()) {
          snapshot.forEach((element) => {
            a = element.val();
          });
          if (a.role == "user" && a.password == creds.password) {
            localStorage.setItem("role", a.role);
            localStorage.setItem("nickname", creds.nickname);
            history.push("/");
          } else if (a.role == "admin" && a.password == creds.password) {
            localStorage.setItem("role", a.role);
            localStorage.setItem("nickname", creds.nickname);
            history.push("/admin");
          } else {
            alert("username or password is incorrect");
          }
          setShowLoading(false);
        } else {
          alert("username or password is incorrect");
          setShowLoading(false);
        }
      });
  };

  const onChange = (e) => {
    e.persist();
    setCreds({ ...creds, [e.target.name]: e.target.value });
  };

  return (
    <>
      <Navbar type="signin" />
      <div className="form-container">
        <form className="form" onSubmit={login}>
          <h1 className="signin">Sign In</h1>
          <div className="form-inputs">
            <label className="form-label">Username</label>
            <input
              className="form-input"
              type="text"
              name="nickname"
              placeholder="Enter your username"
              value={creds.nickname}
              onChange={onChange}
            />
          </div>
          <div className="form-inputs">
            <label className="form-label">Password</label>
            <input
              className="form-input"
              type="password"
              name="password"
              placeholder="Enter your password"
              value={creds.password}
              onChange={onChange}
            />
          </div>
          <button className="form-input-btn" type="submit">
            Sign In
          </button>
          <span className="form-input-login">
            Don&apos;t have an account? Register <Link to="/sign-up">here</Link>
          </span>
        </form>
      </div>
      <Footer />
    </>
  );
}

export default Login;
