import React, { useState } from "react";

class AddField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      service: [{ id: "" }],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  addClick() {
    this.setState((prevState) => ({
      service: [...prevState.service, { id: ""}],
    }));
  }

  createUI() {
    return this.state.service.map((el, i) => (
      <div key={i}>
        <input
          placeholder="ID"
          name="id"
          value={el.id || ""}
          onChange={this.handleChange.bind(this, i)}
        />
        <input
          type="button"
          value="remove"
          onClick={this.removeClick.bind(this, i)}
        />
      </div>
    ));
  }

  handleChange(i, e) {
    const { name, value } = e.target;
    let service = [...this.state.service];
    service[i] = { ...service[i], [name]: value };
    this.setState({ service});
  }

  removeClick(i) {
    let service = [...this.state.service];
    service.splice(i, 1);
    this.setState({ service });
  }

  handleSubmit(event) {
    alert("A name was submitted: " + JSON.stringify(this.state.service));
    event.preventDefault();
  }

  render() {
    return (
      <div>
        {this.createUI()}
        <input
          type="button"
          value="add more"
          onClick={this.addClick.bind(this)}
        />
        <button type="submit" value="Submit" onClick={this.handleSubmit}> Send </button>
      </div>
    );
  }
}

export default AddField;
