import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Moment from "moment";
import firebase from "../../Firebase";
import ScrollToBottom from "react-scroll-to-bottom";
import "../../assets/css/ChatRoomUser.css";
import { IoSend } from "react-icons/io5";

function ChatRoom2() {
  const [chats, setChats] = useState([]);
  const [users, setUsers] = useState([]);
  const [nickname, setNickname] = useState("");
  const [roomname, setRoomname] = useState("");
  const [newchat, setNewchat] = useState({
    roomname: "",
    nickname: "",
    message: "",
    date: "",
    type: "",
  });
  const history = useHistory();
  const room = localStorage.getItem("nickname");

  useEffect(() => {
    const fetchData = async () => {
      setNickname(localStorage.getItem("nickname"));
      setRoomname(room);
      firebase
        .database()
        .ref("chats/")
        .orderByChild("roomname")
        .equalTo(roomname)
        .on("value", (resp) => {
          setChats([]);
          setChats(snapshotToArray(resp));
        });
    };

    fetchData();
  }, [room, roomname]);

  useEffect(() => {
    const fetchData = async () => {
      setNickname(localStorage.getItem("nickname"));
      setRoomname(room);
      firebase
        .database()
        .ref("roomusers/")
        .orderByChild("roomname")
        .equalTo(roomname)
        .on("value", (resp2) => {
          setUsers([]);
          const roomusers = snapshotToArray(resp2);
          setUsers(roomusers.filter((x) => x.status === "online"));
        });
    };

    fetchData();
  }, [room, roomname]);

  const snapshotToArray = (snapshot) => {
    const returnArr = [];

    snapshot.forEach((childSnapshot) => {
      const item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    });

    return returnArr;
  };

  const submitMessage = (e) => {
    e.preventDefault();
    const chat = newchat;
    chat.roomname = roomname;
    chat.nickname = nickname;
    chat.date = Moment(new Date()).format("DD/MM/YYYY HH:mm:ss");
    chat.type = "message";
    const newMessage = firebase.database().ref("chats/").push();
    newMessage.set(chat);
    setNewchat({
      roomname: "",
      nickname: "",
      message: "",
      date: "",
      type: "",
    });
  };

  const onChange = (e) => {
    e.persist();
    setNewchat({ ...newchat, [e.target.name]: e.target.value });
  };

  return (
    <div className="container_user_chat">
      {/* <Col xs='auto'> */}
      <ScrollToBottom className="chat_user_content">
        {chats.map((item, idx) => (
          <div key={idx} className="message-box-user">
            {item.type === "join" || item.type === "exit" ? (
              <div className="ChatStatus">
                <span className="ChatDate">{item.date}</span>
                <span className="ChatContentCenter">{item.message}</span>
              </div>
            ) : (
              <div className="ChatMessage">
                <div
                  className={`${
                    item.nickname === nickname ? "RightBubble" : "LeftBubble"
                  }`}
                >
                  <p>{item.message}</p>
                  <p className="MsgDate">at {item.date}</p>
                </div>
              </div>
            )}
          </div>
        ))}
      </ScrollToBottom>
      <form onSubmit={submitMessage} className="submit-chat">
        <input
          type="text"
          name="message"
          id="message-user"
          placeholder="Ask us for help..."
          value={newchat.message}
          onChange={onChange}
          autoComplete="off"
        />
        <button type="submit">
          <IoSend size="25" style={{ paddingBottom: "5px" }} />
        </button>
      </form>
      {/* </Col> */}
    </div>
  );
}

export default ChatRoom2;
