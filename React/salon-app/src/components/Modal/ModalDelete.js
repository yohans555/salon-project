import Modal from "react-bootstrap/Modal";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import React from "react";

export default function ModalDelete(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const id = props.id;

  async function deletePost() {
    const requestOptions = {
      method: "DELETE",
      headers: {
        Authorization: "Bearer my-token",
        "My-Custom-Header": "foobar",
      },
    };
    const url = props.url;
    await fetch(url, requestOptions);
    alert("Data berhasil di hapus");
    window.location.reload();
  }

  return (
    <>
      <Button className="button-add" variant="danger" onClick={handleShow}>
        Delete
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Delete</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Apakah kamu yakin ingin menghapus item ini?</h5>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="danger" onClick={deletePost}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
