import Modal from "react-bootstrap/Modal";
import { useState } from "react";
import { Button } from "react-bootstrap";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Service from "../../Services/APIServices";
import "../../assets/css/modal.scss";

function handleFormSubmit(event) {
  const form = document.querySelector("form");
  event.preventDefault();
  const data = new FormData(form);

  // const formJSON = Object.fromEntries(data.entries());
  let listCheckBox = [
    "hairCut",
    "coloring",
    "creamBath",
    "waxing",
    "grooming",
    "styling",
  ];
  listCheckBox.forEach((element) => {
    if (data.get(element) == null) {
      data.set(element, false);
    }
  });
  Service.saveData("employee", data);
  alert("Data berhasil disimpan!");
  window.location.reload();
}

export default function ModalEmployee(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <Button className="button-add" onClick={handleShow}>
        Add New Employee
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Employee</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className="form">
            <div className="input-group">
              <label>First Name</label>
              <input
                id="firstName"
                name="firstName"
                type="text"
                placeholder="first Name"
              />
            </div>
            <div className="input-group">
              <label>Last Name</label>
              <input
                id="lastName"
                name="lastName"
                type="text"
                placeholder="last name"
              />
            </div>
            <div className="input-group">
              <label>Gender</label>
              <select id="gender" name="gender">
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
            <div className="input-group">
              <label>Birth Date</label>
              <input
                id="birthDate"
                name="birthdate"
                type="date"
                placeholder="Birth Date"
              />
            </div>
            <div className="input-group">
              <label>ID Card</label>
              <input
                id="idcard"
                name="idCard"
                type="text"
                placeholder="ID Card"
              />
            </div>
            <div className="input-group">
              <label>Phone</label>
              <input
                id="phone"
                name="phone"
                type="number"
                placeholder="Phone"
              />
            </div>
            <div className="input-group">
              <label>Email</label>
              <input id="email" name="email" type="email" placeholder="Email" />
            </div>
            <div className="input-group">
              <label>Address</label>
              <input
                id="address"
                name="address"
                type="text"
                placeholder="Email"
              />
            </div>
            <div className="row">
              <div className="col">
                <input
                  id="styling"
                  name="styling"
                  className="form-check-input"
                  type="checkbox"
                  value={true}
                />
                <h5>Styling</h5>
              </div>
              <div className="col">
                <input
                  id="creamBath"
                  name="creamBath"
                  className="form-check-input"
                  type="checkbox"
                  value={true}
                  defaultValue={false}
                />
                <h5>Cream Bath</h5>
              </div>
              <div className="row">
                <div className="col">
                  <input
                    id="waxing"
                    name="waxing"
                    className="form-check-input"
                    type="checkbox"
                    value={true}
                  />
                  <h5>Waxing</h5>
                </div>
                <div className="col">
                  <input
                    id="coloring"
                    name="coloring"
                    className="form-check-input"
                    type="checkbox"
                    value={true}
                  />
                  <label>Coloring</label>
                </div>
              </div>

              <div className="row">
                <div className="col">
                  <input
                    id="hairCut"
                    name="hairCut"
                    className="form-check-input"
                    type="checkbox"
                    value={true}
                  />
                  <label>Haircut</label>
                </div>
                <div className="col">
                  <input
                    id="grooming"
                    name="gromiing"
                    className="form-check-input"
                    type="checkbox"
                    value={true}
                  />
                  <h5>Grooming</h5>
                </div>
              </div>
            </div>

            <div className="input-group">
              <label id="imgs">Upload Image</label>
              <input
                id="img"
                name="profilePicture"
                type="file"
                accept="image/*"
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button type="submit" variant="primary" onClick={handleFormSubmit}>
            Send
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
