import Modal from "react-bootstrap/Modal";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/modal.scss";

function send(e) {
  var sel = document.getElementById("category");
  var text = sel.options[sel.selectedIndex].text;
  var ids = document.getElementById("category").value;
  let post = {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=UTF-8",
    },
    body: JSON.stringify({
      id: 123,
      name: document.getElementById("name").value,
      price: document.getElementById("price2").value,
      serviceCategory: {
        id: ids,
        name: text,
      },
    }),
  };
  let send = fetch("http://localhost:8080/save/services", post);
  alert("Data berhasil terkirim");
  window.location.reload();
}

function ModalService(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button className="button-add" onClick={handleShow}>
        Add New Service
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Add Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <select id="category">
              {props.opt.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              ))}
            </select>
          </div>
          <div>
            <label>Name</label>
            <div>
              <input type="text" id="name" placeholder="Input name" />
            </div>
          </div>
          <div>
            <label>Price</label>
            <div>
              <input type="text" id="price2" placeholder="Input Price" />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" className="send" onClick={send}>
            Send
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalService;
