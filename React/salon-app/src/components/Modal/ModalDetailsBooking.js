import Modal from "react-bootstrap/Modal";
import { useState } from "react";
import { Button } from "react-bootstrap";
import React from "react";
import DetailsBookingList from "components/Details/DetailsBookingList";
import "../../assets/css/modal.scss";

export default function ModalDetailsBooking(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const item = props.item;
  const id = props.id;

  return (
    <>
      <Button className="button-add" onClick={handleShow}>
        Details
      </Button>
      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <DetailsBookingList item={item} id={id} />
        </Modal.Body>
      </Modal>
    </>
  );
}
