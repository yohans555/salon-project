import Modal from "react-bootstrap/Modal";
import { useState } from "react";
import { Button } from "react-bootstrap";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Service from "../../Services/APIServices";

function handleFormSubmit(event) {
  const form = document.querySelector("form");
  event.preventDefault();
  const data = new FormData(form);
  Service.saveData("product", data);
  alert("Data berhasil disimpan!");
  window.location.reload();
}

export default function ModalProduct(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <Button className="button-add" onClick={handleShow}>
        Add New Product
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form className="form-product">
            <div className="input-group">
              <label>Product Code</label>
              <input
                id="productCode"
                name="productCode"
                type="text"
                placeholder="Product Code"
              />
            </div>
            <div className="input-group">
              <label>Category</label>
              <select id="category" name="serviceCategory">
                {props.opt.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="input-group">
              <label>Brand</label>
              <input id="brand" name="brand" type="text" placeholder="Brand" />
            </div>
            <div className="input-group">
              <label>Name</label>
              <input id="name" name="name" type="text" placeholder="Name" />
            </div>
            <div className="input-group">
              <label>Price</label>
              <input
                id="price-service"
                name="price"
                type="text"
                placeholder="Price"
              />
            </div>
            <div className="input-group">
              <label id="imgs">Upload Image</label>
              <input
                id="picture"
                name="picture"
                type="file"
                accept="image/*"
                multiple
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button type="submit" variant="primary" onClick={handleFormSubmit}>
            Send
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
