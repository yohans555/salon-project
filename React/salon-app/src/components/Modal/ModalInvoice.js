import Modal from "react-bootstrap/Modal";
import { useState } from "react";
import { Button } from "react-bootstrap";
import React from "react";
import HandlerServices from "Services/HandlerServices";

export default function ModalInvoice(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const added = props.added;
  const dataService = props.dataService;

  const cart = HandlerServices.getCart(props.added, props.dataService);
  const aa = () => console.log(`object`, added, dataService);

  return (
    <>
      <Button className="button-add" onClick={aa}>
        Invoice
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>{props.total}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {cart.map((res) => (
            <h1 key={res.id}>{res.id}</h1>
          ))}
        </Modal.Body>
      </Modal>
    </>
  );
}
