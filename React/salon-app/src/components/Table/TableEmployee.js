import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../../assets/vendor/animate/animate.css";
import "../../assets/vendor/select2/select2.min.css";
import "../../assets/vendor/perfect-scrollbar/perfect-scrollbar.css";
import "../../assets/css/util.css";
import "../../assets/css/main.css";
import ModalEmployee from "components/Modal/ModalEmployee";
import ModalDetailsEmployee from "components/Modal/ModalDetailsEmployee";
import Service from "../../Services/APIServices";

class TableEmployee extends React.Component {
  state = {
    data: [],
  };

  //fetching data from DB
  async componentDidMount() {
    const response = await fetch("http://localhost:8080/get/employee");
    const body = await response.json();
    this.setState({ data: body });
  }

  delete(id) {
    let confirms = confirm("Apakah anda yakin ingin menghapus item ini?");
    if (confirms == true) {
      Service.deleteData("employee", id);
      window.location.reload();
    } else {
      window.location.reload();
    }
  }

  render() {
    const { data } = this.state;
    return (
      <div className="container-table100">
        <ModalEmployee />
        <div className="wrap-table100">
          <div className="table100 ver1 m-b-110">
            <div className="table100-head">
              <table>
                <thead>
                  <tr className="row100 head">
                    <th className="cell100 column1">Name</th>
                    <th className="cell100 column2">Gender</th>
                    <th className="cell100 column3">Birth Date</th>
                    <th className="cell100 column4">Actions</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div className="table100-body js-pscroll">
              <table>
                <tbody>
                  <tr className="row100 body">
                    {data.map((item) => (
                      <table key={item.id}>
                        <td className="cell100 column1">{item.firstName}</td>
                        <td className="cell100 column2">{item.gender}</td>
                        <td className="cell100 column3">{item.birthdate}</td>
                        <td className="cell100 column4">
                          <ModalDetailsEmployee id={item} />
                          <button
                            className="btn btn-danger"
                            onClick={() => this.delete(item.id)}
                          >
                            Delete
                          </button>
                        </td>
                      </table>
                    ))}
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TableEmployee;
