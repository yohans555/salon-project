import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../../assets/vendor/animate/animate.css";
import "../../assets/vendor/select2/select2.min.css";
import "../../assets/vendor/perfect-scrollbar/perfect-scrollbar.css";
import "../../assets/css/util.css";
import "../../assets/css/main.css";
import ModalDetailsBooking from "components/Modal/ModalDetailsBooking";

class TableHistory extends React.Component {
    state = {
        data: [],
        services: [],
    };

    async componentDidMount() {
        const response = await fetch("http://localhost:8080/get/transaction");
        const body = await response.json();
        this.setState({ data: body });
    }

    render() {
        const { data } = this.state;
        return (
            <>
                <h3>Booking History</h3>
                <div className='container-table100'>
                    <div className='wrap-table100'>
                        <div className='table100 ver1 m-b-110'>
                            <div className='table100-head'>
                                <table>
                                    <thead>
                                        <tr className='row100 head'>
                                            <th className='cell100 column1'>
                                                Date
                                            </th>
                                            <th className='cell100 column2'>
                                                Name
                                            </th>
                                            <th className='cell100 column3'>
                                                Email
                                            </th>
                                            <th className='cell100 column4'>
                                                Phone
                                            </th>
                                            <th className='cell100 column5'>
                                                Details
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div className='table100-body js-pscroll'>
                                <table>
                                    <tbody>
                                        <tr className='row100 body'>
                                            {data.map((item, index) => (
                                                <table key={item.id}>
                                                    <td className='cell100 column1'>
                                                        {item.date}
                                                    </td>
                                                    <td className='cell100 column2'>
                                                        {item.username}
                                                    </td>
                                                    <td className='cell100 column3'>
                                                        {item.email}
                                                    </td>
                                                    <td className='cell100 column4'>
                                                        08{item.phone}
                                                    </td>
                                                    <td className='cell100 column5'>
                                                        <ModalDetailsBooking
                                                            item={item}
                                                            id={index + 1}
                                                        />
                                                    </td>
                                                </table>
                                            ))}
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default TableHistory;
