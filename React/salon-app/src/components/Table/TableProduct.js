import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../../assets/vendor/animate/animate.css";
import "../../assets/vendor/select2/select2.min.css";
import "../../assets/vendor/perfect-scrollbar/perfect-scrollbar.css";
import "../../assets/css/util.css";
import "../../assets/css/main.css";
import ModalDelete from "components/Modal/ModalDelete";
import ModalProduct from "components/Modal/ModalProduct";
import ModalDetailsProduct from "components/Modal/ModalDetailsProduct";

class TableProductNew extends React.Component {
  state = {
    data: [],
    category: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/get/product");
    const body = await response.json();
    this.setState({ data: body });

    const responseService = await fetch(
      "http://localhost:8080/get/service_category"
    );
    const bodyService = await responseService.json();
    this.setState({ category: bodyService });
  }

  render() {
    const { data, category } = this.state;
    return (
      <div className="container-table100">
        <div className="wrap-table100">
          <ModalProduct opt={category} />
          <div className="table100 ver1 m-b-110">
            <div className="table100-head">
              <table>
                <thead>
                  <tr className="row100 head">
                    <th className="cell100 column1">Category</th>
                    <th className="cell100 column2">Code</th>
                    <th className="cell100 column3">Brand</th>
                    <th className="cell100 column4">Name</th>
                    <th className="cell100 column5">Price</th>
                    <th className="cell100 column6">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div className="table100-body js-pscroll">
              <table>
                <tbody>
                  <tr className="row100 body">
                    {data.map((item, index) => (
                      <table key={item.id}>
                        <td className="cell100 column1">
                          {item.serviceCategory.name}
                        </td>
                        <td className="cell100 column2">{item.productCode}</td>
                        <td className="cell100 column3">{item.brand}</td>
                        <td className="cell100 column4">{item.name}</td>
                        <td className="cell100 column5">Rp. {item.price}</td>
                        <td className="cell100 column6">
                          <ModalDetailsProduct id={item} />
                          <ModalDelete
                            id={item.id}
                            url={
                              "http://localhost:8080/delete/product/" + item.id
                            }
                          />
                        </td>
                      </table>
                    ))}
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TableProductNew;
