import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "../../assets/vendor/animate/animate.css";
import "../../assets/vendor/select2/select2.min.css";
import "../../assets/vendor/perfect-scrollbar/perfect-scrollbar.css";
import "../../assets/css/util.css";
import "../../assets/css/main.css";
import ModalService from "components/Modal/ModalService";
import ModalDetails from "components/Modal/ModalDetails";
import Service from "../../Services/APIServices";

class TableService extends React.Component {
  state = {
    data: [],
    category: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/get/service");
    const body = await response.json();
    this.setState({ data: body });

    const responseService = await fetch(
      "http://localhost:8080/get/service_category"
    );
    const bodyService = await responseService.json();
    this.setState({ category: bodyService });
  }

  delete(id) {
    alert("sukses");
    Service.deleteData("services", id);
    window.location.reload();
  }

  render() {
    const { data, category } = this.state;
    return (
      <div className="container-table100">
        <ModalService opt={category} />
        <div className="wrap-table100">
          <div className="table100 ver1 m-b-110">
            <div className="table100-head">
              <table>
                <thead>
                  <tr className="row100 head">
                    <th className="cell100 column1">Category</th>
                    <th className="cell100 column2">Name</th>
                    <th className="cell100 column3">Price</th>
                    <th className="cell100 column4">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
            <div className="table100-body js-pscroll">
              <table>
                <tbody>
                  <tr className="row100 body">
                    {data.map((item) => (
                      <table key={item.id}>
                        {/* <td id="id" hidden="true">
                          {item.id}
                        </td> */}
                        <td className="cell100 column1">
                          {item.serviceCategory.name}
                        </td>
                        <td className="cell100 column2">{item.name}</td>
                        <td className="cell100 column3">Rp. {item.price}</td>
                        <td className="cell100 column3">
                          <ModalDetails id={item} />
                          <button
                            className="btn btn-danger"
                            onClick={() => this.delete(item.id)}
                          >
                            Delete
                          </button>
                        </td>
                      </table>
                    ))}
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TableService;
