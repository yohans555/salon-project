import React from "react";
import Service from "../../Services/APIServices";

class DetailsServices extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.id;
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  saveData(state) {
    Service.saveData("services", this.state);
    window.location.reload();
  }

  render() {
    return (
      <>
        <label>Category</label>
        <input
          id="ids"
          name="serviceCategory.id"
          onChange={this.handleChange}
          value={this.state.serviceCategory.name}
          type="text"
        ></input>
        <label>Name</label>
        <input
          id="name"
          name="name"
          value={this.state.name}
          onChange={this.handleChange}
          type="text"
        ></input>
        <div>
          <label>Price</label>
        </div>
        <input
          id="price-service"
          name="price"
          value={this.state.price}
          onChange={this.handleChange}
          type="number"
        ></input>
        <button
          className="btn btn-primary"
          id="save-employee"
          onClick={() => this.saveData(this.state)}
        >
          Save
        </button>
      </>
    );
  }
}

export default DetailsServices;
