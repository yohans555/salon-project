import React from "react";
import Service from "../../Services/APIServices";

class DetailsServices extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.id;
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  saveData(event) {
    event.preventDefault();
    const form = document.querySelector("form");
    const data = new FormData(form);
    Service.saveData("product", data);
    window.location.reload();
  }

  render() {
    return (
      <>
        <form className="form-product" encType="multipart/form-data">
          <input
            id="id"
            name="id"
            onChange={this.handleChange}
            value={this.state.id}
            type="text"
            hidden
          ></input>
          <label id="label-product">Product Code</label>
          <input
            id="productCode"
            name="productCode"
            onChange={this.handleChange}
            value={this.state.productCode}
            type="text"
          ></input>
          <label id="label-product">Name</label>
          <input
            id="name"
            name="name"
            value={this.state.name}
            onChange={this.handleChange}
            type="text"
          ></input>
          <label id="label-product">Brand</label>
          <input
            id="brand"
            name="brand"
            value={this.state.brand}
            onChange={this.handleChange}
            type="text"
          ></input>
          <div>
            <label id="label-product">Price</label>
          </div>
          <select id="valutas">
            <option value="IDR">IDR</option>
            <option value="USD">USD</option>
          </select>
          <input
            id="price-service"
            name="price"
            value={this.state.price}
            onChange={this.handleChange}
            type="number"
          ></input>
          <input
            id="serviceCategory"
            name="serviceCategory"
            value={this.state.serviceCategory.id}
            onChange={this.handleChange}
            type="text"
            hidden={true}
          ></input>

          <div className="input-group">
            <label id="imgs">Upload Image</label>
            <input
              id="picture"
              name="picture"
              onChange={this.handleChange}
              type="file"
              accept="image/*"
            />
          </div>
          <label id="label-product">Pictures</label>
          <img src={this.state.picture}></img>
        </form>
        <button
          className="btn btn-primary"
          id="save-employee"
          onClick={this.saveData}
        >
          Save
        </button>
      </>
    );
  }
}

export default DetailsServices;
