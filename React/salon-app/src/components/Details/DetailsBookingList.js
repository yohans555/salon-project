import moment from "moment";
import React from "react";
import Service from "../../Services/APIServices";
import "../../assets/css/modal.scss";

const dateNow = new Date();
const dateStringTitle = moment(dateNow).format("YYYY-MM-DD");

class DetailsBookingList extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.item;
    this.id = this.props.id;
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  saveData(event) {
    const data = document.querySelector("form");
    const form = new FormData(data)
    Service.saveData("transaction", this.state);
    alert("Data berhasil diedit!");
    window.location.reload();
  }

  render() {
    return (
      <div className="modal-details">
        <form>
          <input name="id" value={this.state.id} hidden></input>
          <label>Username</label>
          <input
            name="username"
            value={this.state.username}
            onChange={this.handleChange}
            type="text"
            readOnly
          ></input>
          <label>Date</label>
          <input
            name="date"
            value={this.state.date}
            onChange={this.handleChange}
            type="date"
            min={dateStringTitle}
          ></input>
          <label>Guest Name</label>
          <input
            name="guestName"
            value={this.state.guestName}
            onChange={this.handleChange}
            type="text"
          ></input>
          <label>Email</label>
          <input
            name="email"
            value={this.state.email}
            onChange={this.handleChange}
            type="email"
          ></input>
          <label>Phone</label>
          <input id="identity" value="08" type="number" readOnly></input>
          <input
            name="phone"
            value={this.state.phone}
            onChange={this.handleChange}
            type="number"
          ></input>
          <label>Services</label>
          {this.state.services.map((item, index) => (
            <input
              name="services"
              key={item.id}
              value={
                this.state.services[index].serviceCategory.name +
                " " +
                "-" +
                " " +
                this.state.services[index].name
              }
              onChange={this.handleChange}
              type="text"
              readOnly
            />
          ))}
          <label>Status</label>
          <select
            name="status"
            onChange={this.handleChange}
            type="email"
            id="status-select"
          >
            <option selected>{this.state.status}</option>
            <option value="paid">Paid</option>
            <option value="unpaid">Unpaid</option>
          </select>
        </form>

        <button
          id="save-employ"
          className="btn btn-primary"
          onClick={() => this.saveData(this.state)}
        >
          Save
        </button>
      </div>
    );
  }
}

export default DetailsBookingList;
