import React from "react";
import Service from "../../Services/APIServices";

class DetailsEmployee extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.id;
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  saveData() {
    Service.saveData("emp", this.state);
    alert("Data berhasil diedit!");
    window.location.reload();
  }

  render() {
    return (
      <>
        <label>First Name</label>
        <input
          name="firstName"
          value={this.state.firstName}
          onChange={this.handleChange}
          type="text"
        ></input>
        <label>Last Name</label>
        <input
          name="lastName"
          value={this.state.lastName}
          onChange={this.handleChange}
          type="text"
        ></input>
        <label>Birth Date</label>
        <input
          name="birthdate"
          value={this.state.birthdate}
          onChange={this.handleChange}
          type="date"
        ></input>
        <label>ID Card</label>
        <input
          name="idCard"
          value={this.state.idCard}
          onChange={this.handleChange}
          type="text"
        ></input>
        <label>Phone</label>
        <input type="number" value="08" id="identity" readOnly />
        <input
          name="phone"
          value={this.state.phone}
          onChange={this.handleChange}
          type="number"
        ></input>
        <label>Email</label>
        <input
          name="email"
          value={this.state.email}
          onChange={this.handleChange}
          type="email"
        ></input>
        <label>Address</label>
        <input
          name="address"
          value={this.state.address}
          onChange={this.handleChange}
          type="text"
        ></input>

        <label>Services</label>
        <div className="row">
          <div className="col-sm">
            <input
              type="checkbox"
              checked={this.state.hairCut}
              value={this.state.hairCut}
            ></input>
            <h5>Hair Cut</h5>
          </div>
          <div className="col-sm">
            <input
              type="checkbox"
              checked={this.state.grooming}
              value={this.state.grooming}
            ></input>
            <h5>Grooming</h5>
          </div>
          <div className="col-sm">
            <input
              type="checkbox"
              checked={this.state.creamBath}
              value={this.state.creamBath}
            ></input>
            <h5>Cream Bath</h5>
          </div>
        </div>

        <div className="row">
          <div className="col-sm">
            <input
              type="checkbox"
              checked={this.state.coloring}
              value={this.state.coloring}
            ></input>
            <h5>Coloring</h5>
          </div>

          <div className="col-sm">
            <input
              type="checkbox"
              checked={this.state.waxing}
              value={this.state.waxing}
            ></input>
            <h5>Waxing</h5>
          </div>
          <div className="col-sm">
            <input
              type="checkbox"
              checked={this.state.styling}
              value={this.state.styling}
            ></input>
            <h5>Styling</h5>
          </div>
        </div>
        <label>Pictures</label>
        <img src={this.state.profilePicture}></img>
        <button
          id="save-employ"
          className="btn btn-primary"
          onClick={() => this.saveData(this.state)}
        >
          Save
        </button>
      </>
    );
  }
}

export default DetailsEmployee;
