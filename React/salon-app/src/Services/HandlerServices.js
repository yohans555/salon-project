class HandlerService {
    getPrice(idx, array) {
        let price = 0;
        idx.forEach((item) => {
            array.forEach((element) => {
                if (element.id == item.id) {
                    price = price + element.price;
                }
            });
        });

        return parseInt(price);
    }

    getCart(idx, array) {
        let cart = [];

        return cart;
    }

    findElement(arr, propName, propValue) {
        for (var i = 0; i < arr.length; i++)
            if (arr[i][propName] == propValue) return arr[i];
    }
}

export default new HandlerService();
