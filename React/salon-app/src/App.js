import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useLocation,
} from "react-router-dom";
import Login from "./components/Login";
import PropTypes from "prop-types";
import Admin from "./layouts/Admin";
import User from "./layouts/User";
import Products from "./pages/PublicPage/Products";
import Home from "./pages/PublicPage/Home";
import PriceList from "./pages/PublicPage/PriceList";
import SignUpPage from "pages/PublicPage/SignUpPage";

function App() {
  let location = useLocation();

  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/services" component={PriceList} />
        <Route path="/products" component={Products} />
        <Route path="/sign-up" component={SignUpPage} />
        <Route path="/sign-in">
          <Login />
        </Route>
        <SecureRouteAdmin path="/admin">
          <Admin />
        </SecureRouteAdmin>
        <SecureRoute path="/user">
          <User />
        </SecureRoute>
      </Switch>
    </Router>
  );
}

App.propTypes = {
  children: PropTypes.any.isRequired,
};

export default App;

function SecureRouteAdmin({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem("role") == "admin" ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/sign-in",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

function SecureRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem("role") == "user" ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/sign-in",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
