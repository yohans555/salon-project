import React, { Component } from "react";
import Footer from "userComponents/Footer";
import Navbar from "../../userComponents/Navbar/Navbar";
import UserNavbar from "userComponents/Navbar/UserNavbar";
import AccordionPriceList from "userComponents/PriceList/AccordionPriceList";
import ChatBox from "userComponents/ChatBox";

export default class PriceList extends Component {
  render() {
    if (localStorage.getItem("role") === "user") {
      return (
        <>
          <UserNavbar />
          <AccordionPriceList />
          <Footer />
          <ChatBox />
        </>
      );
    } else {
      return (
        <>
          <Navbar />
          <AccordionPriceList />
          <Footer />
          <ChatBox />
        </>
      );
    }
  }
}
