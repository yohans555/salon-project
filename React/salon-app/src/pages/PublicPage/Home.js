import React from "react";
import Cards from "../../userComponents/Cards";
import HeroSection from "../../userComponents/HeroSection";
import Footer from "../../userComponents/Footer";
import Navbar from "../../userComponents/Navbar/Navbar";
import UserNavbar from "userComponents/Navbar/UserNavbar";
import ChatBox from "userComponents/ChatBox";

function Home() {
  if (localStorage.getItem("role") === "user") {
    return (
      <>
        <UserNavbar />
        <HeroSection />
        <Cards path="/price-list" />
        <Footer />
        <ChatBox />
      </>
    );
  } else {
    return (
      <>
        <Navbar />
        <HeroSection />
        <Cards path="/price-list" />
        <Footer />
        <ChatBox />
      </>
    );
  }
}

export default Home;
