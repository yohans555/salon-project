import React, { Component } from "react";
import Navbar from "../../userComponents/Navbar/Navbar";
import Footer from "userComponents/Footer";
import "../../userComponents/Cards.css";
import CardItem from "userComponents/CardItem";
import UserNavbar from "userComponents/Navbar/UserNavbar";
import ChatBox from "userComponents/ChatBox";
import Service from "../../Services/APIServices";
import { Redirect } from "react-router-dom";

export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productsData: [],
        };
    }

    // fetch data product
    componentDidMount() {
        Service.getData("product").then((res) => {
            const body = res.data;
            this.setState({ productsData: body });
        });
    }

    // change image data string into array
    getProduct = () => {
        let products = [];
        let imageList = new Array();
        this.state.productsData.map((data) => {
            imageList = data.picture.split(";");
            products.push({
                label: data.serviceCategory.name,
                brand: data.brand,
                name: data.name,
                image: imageList.filter(function (e) {
                    return e === 0 || e;
                }),
            });
        });
        return products;
    };

    // get role
    getRole = () => {
        let role = localStorage.getItem("role");
        return role;
    };

    render() {
        if (this.getRole() === "user") {
            return (
                <>
                    <UserNavbar />
                    <div className='cards'>
                        <h1>Products We Use</h1>
                        <div className='cards__container'>
                            <ul className='cards__items'>
                                {this.getProduct().map((data, key) => (
                                    <CardItem
                                        key={key}
                                        src={data.image[0]}
                                        label={data.label}
                                        text={data.brand + " - " + data.name}
                                    />
                                ))}
                            </ul>
                        </div>
                    </div>
                    <Footer />
                    <ChatBox />
                </>
            );
        } else {
            return (
                <>
                    <Navbar />
                    <div className='cards'>
                        <h1>Products We Use</h1>
                        <div className='cards__container'>
                            <ul className='cards__items'>
                                {this.getProduct().map((data, key) => (
                                    <CardItem
                                        key={key}
                                        src={data.image[0]}
                                        label={data.label}
                                        text={data.brand + " - " + data.name}
                                    />
                                ))}
                            </ul>
                        </div>
                    </div>
                    <Footer />
                    <ChatBox />
                </>
            );
        }
    }
}
