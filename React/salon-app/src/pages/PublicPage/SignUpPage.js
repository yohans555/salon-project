import React, { Component } from "react";
import Footer from "../../userComponents/Footer";
import SignUp from "../../userComponents/signin-register/SignUp";
import Navbar from "../../userComponents/Navbar/Navbar";

export default class SignUpPage extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <SignUp />
        <Footer />
      </div>
    );
  }
}
