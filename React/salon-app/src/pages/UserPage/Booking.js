import React, { Component } from "react";
import BookingForm from "userComponents/Booking/BookingForm";
import ChatBox from "userComponents/ChatBox";
import Footer from "userComponents/Footer";

export default class Booking extends Component {
  render() {
    return (
      <>
        <BookingForm />
        <Footer />
        <ChatBox />
      </>
    );
  }
}
