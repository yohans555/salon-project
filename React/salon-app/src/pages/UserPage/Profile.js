import React, { Component } from "react";
import ChatBox from "userComponents/ChatBox";
import Footer from "userComponents/Footer";
import MyProfileForm from "userComponents/MyProfileForm";

export default class Profile extends Component {
  render() {
    return (
      <>
        <MyProfileForm />
        <Footer />
        <ChatBox />
      </>
    );
  }
}
