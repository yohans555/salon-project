import React, { Component } from "react";
import ChatBox from "userComponents/ChatBox";
import Footer from "userComponents/Footer";
import TableHistory from "userComponents/TableHistory";

export default class HistoryTransaction extends Component {
  render() {
    return (
      <>
        <TableHistory />
        <Footer />
        <ChatBox />
      </>
    );
  }
}
