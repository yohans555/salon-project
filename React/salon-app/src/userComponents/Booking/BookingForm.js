import React, { Component } from "react";
import "../MyProfileForm.css";
import Service from "../../Services/APIServices";
import firebase from "../../Firebase";
import moment from "moment";
import "./BookingForm.css";
import HandlerService from "../../Services/HandlerServices";
import ConvertToRupiah from "userComponents/ConvertToRupiah";

export default class BookingForm extends Component {
    constructor(props) {
        super(props);
        this.total = 0;
        this.state = {
            userBooking: {},
            serviceOptions: [],
            dataEmployee: [],
            service: [{ id: "" }],
        };
    }

    async componentDidMount() {
        // get services
        const responseService = await fetch(
            "http://localhost:8080/get/service"
        );
        const bodyService = await responseService.json();
        this.setState({ serviceOptions: bodyService });

        // get employee
        const responseEmployee = await fetch(
            "http://localhost:8080/get/employee"
        );
        const bodyEmployee = await responseEmployee.json();
        this.setState({ dataEmployee: bodyEmployee });

        // get user profile
        const userRef = firebase.database().ref("users/");
        userRef.on("value", (snapshot) => {
            const users = snapshot.val();
            const userList = [];
            for (let id in users) {
                userList.push({ id, ...users[id] });
            }
            const userNow = localStorage.getItem("nickname");
            const user = userList.find((data) => data.nickname == userNow);
            this.setState((prevState) => ({
                ...prevState,
                userBooking: {
                    fullname: user.fullname,
                    nickname: user.nickname,
                    email: user.email,
                    phone: user.phone,
                },
            }));
        });
    }

    getService = () => {
        return this.state.service.map((el, i) => (
            <div key={i} className='services-book-form'>
                <select
                    className='input-services-box'
                    onChange={this.handleChangeService.bind(this, i)}
                    name='id'
                >
                    <option value='starter' selected hidden>
                        --- Choose your services ---
                    </option>
                    {this.state.serviceOptions.map((item) => (
                        <option key={i} value={item.id}>
                            {item.serviceCategory.name} : {item.name} &nbsp; (
                            {ConvertToRupiah(item.price)})
                        </option>
                    ))}
                </select>
                <input
                    type='button'
                    className='remove-services'
                    value='—'
                    onClick={this.removeClick}
                />
            </div>
        ));
    };

    handleChangeService(i, e) {
        const { name, value } = e.target;
        let service = [...this.state.service];
        service[i] = { ...service[i], [name]: value };
        this.setState({ service });
        let price = HandlerService.getPrice(service, this.state.serviceOptions);
        this.total = price;
    }

    addClick = () => {
        this.setState((prevState) => ({
            service: [...prevState.service, { id: "" }],
        }));
        let price = HandlerService.getPrice(
            this.state.service,
            this.state.serviceOptions
        );
        this.total = price;
    };

    removeClick = (i) => {
        let service = [...this.state.service];
        service.splice(i, 1);
        this.setState({ service });
        let price = HandlerService.getPrice(service, this.state.serviceOptions);
        this.total = price;
    };

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState((prevState) => ({
            userBooking: {
                ...prevState.userBooking,
                [name]: value,
            },
        }));
    };

    handleSubmit = (event) => {
        event.preventDefault();
        let userData = this.state.userBooking;
        let data = {
            username: userData.nickname,
            date: userData.date,
            guestName: userData.fullname,
            phone: userData.phone,
            email: userData.email,
            status: "unpaid",
            services: this.state.service,
            employee: { id: userData.id },
            total: this.total,
        };
        Service.saveData("transaction", data);
        alert("booking success!");
    };

    getToday = () => {
        const dateNow = new Date();
        const dateStringMin = moment(dateNow).format("YYYY-MM-DD");
        return dateStringMin;
    };

    get30Day = () => {
        let date = new Date();
        date.setDate(date.getDate() + 30);
        let dateStringMax = moment(date).format("YYYY-MM-DD");
        return dateStringMax;
    };

    render() {
        return (
            <div className='container-profile'>
                <div className='title'>New Booking</div>
                <div className='content'>
                    <form id='profile-form' onSubmit={this.handleSubmit}>
                        <div className='user-details'>
                            <div className='input-box'>
                                <span className='details'>Full Name</span>
                                <input
                                    type='text'
                                    placeholder='Enter your name'
                                    id='input-profile'
                                    name='fullname'
                                    value={this.state.userBooking.fullname}
                                    onChange={this.handleChange}
                                    readOnly
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Username</span>
                                <input
                                    type='text'
                                    placeholder='Enter your username'
                                    id='input-profile'
                                    name='nickname'
                                    value={this.state.userBooking.nickname}
                                    onChange={this.handleChange}
                                    readOnly
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Email</span>
                                <input
                                    type='text'
                                    placeholder='Enter your email'
                                    id='input-profile'
                                    name='email'
                                    value={this.state.userBooking.email}
                                    onChange={this.handleChange}
                                    readOnly
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Phone Number</span>
                                <input
                                    type='text'
                                    placeholder='Enter your number'
                                    id='input-profile'
                                    name='phone'
                                    value={this.state.userBooking.phone}
                                    onChange={this.handleChange}
                                    readOnly
                                />
                            </div>
                            <div className='services-box'>
                                <span className='details'>Choose Services</span>
                                {this.getService()}
                                <input
                                    type='button'
                                    className='button-services'
                                    value='Add Service'
                                    onClick={this.addClick}
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Choose Stylish</span>
                                <select
                                    id='input-profile'
                                    name='id'
                                    value={this.state.userBooking.id}
                                    onChange={this.handleChange}
                                >
                                    <option selected hidden>
                                        --- Select Stylish ---
                                    </option>
                                    {this.state.dataEmployee.map((item) => (
                                        <option key={item.id} value={item.id}>
                                            {item.firstName}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className='input-box'>
                                <span className='details'>Pick Date</span>
                                <input
                                    type='date'
                                    id='input-profile'
                                    name='date'
                                    min={this.getToday()}
                                    max={this.get30Day()}
                                    value={this.state.userBooking.date}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className='services-box'>
                            <p className='total-booking'>Total Price:</p>
                            <p className='price-booking'>
                                {ConvertToRupiah(this.total)}
                            </p>
                        </div>
                        <div className='button'>
                            <input type='submit' value='Book My Reservation' />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
