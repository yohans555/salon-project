import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import "./SignUp.css";
import firebase from "../../Firebase";

const initialState = {
  nameError: "",
  emailError: "",
  phoneError: "",
  passwordError: "",
  password2Error: "",
};

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = { role: "user" };
  }

  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value });
  };

  handleKeyDown = (e) => {
    if (e.key === " ") {
      e.preventDefault();
    }
  };

  handleChangeNoSpace = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    if (value.includes(" ")) {
      value = value.replace(/\s/g, "");
    }
    this.setState({ [name]: value });
  };

  isValidate = () => {
    let nameError = "";
    let emailError = "";
    let passwordError = "";
    let password2Error = "";
    let phoneError = "";
    let checkEmail = [];
    let checkUser = [];
    const ref = firebase.database().ref("users/");

    ref
      .orderByChild("nickname")
      // .equalTo(this.state.nickname)
      .on("child_added", (snapshot) => {
        let user = snapshot.val().nickname;
        checkUser.push(user);
      });

    ref
      .orderByChild("email")
      // .equalTo(this.state.email)
      .on("child_added", (snapshot) => {
        let user = snapshot.val().email;
        checkEmail.push(user);
      });

    if (!this.state.nickname) {
      nameError = "Username is required.";
    }

    if (checkUser.indexOf(this.state.nickname) !== -1) {
      nameError = "Username is already taken. Please choose another name.";
    }

    if (!/^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/.test(this.state.phone)) {
      phoneError = "Phone number is invalid.";
    }

    if (!this.state.email) {
      emailError = "Email address is required.";
    }

    if (checkEmail.indexOf(this.state.email) !== -1) {
      emailError = "Email is already registered.";
    }

    if (!/\S+@\S+\.\S+/.test(this.state.email)) {
      emailError = "Email address is invalid.";
    }

    if (!this.state.password) {
      passwordError = "Password is required.";
    } else if (this.state.password.length < 6) {
      passwordError = "Password needs to be 6 characters or more.";
    }

    if (!this.state.password2) {
      password2Error = "Password is required.";
    }

    if (this.state.password2 !== this.state.password) {
      password2Error = "Passwords do not match.";
    }

    if (
      emailError ||
      nameError ||
      passwordError ||
      password2Error ||
      phoneError
    ) {
      this.setState({
        emailError,
        nameError,
        password2Error,
        passwordError,
        phoneError,
      });
      return false;
    }

    return true;
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const isValid = this.isValidate();

    if (isValid) {
      const newUser = firebase.database().ref("users/").push();
      newUser.set({
        nickname: this.state.nickname,
        email: this.state.email,
        phone: this.state.phone,
        password: this.state.password,
        password2: this.state.password2,
        role: this.state.role,
        address: "",
        fullname: "",
      });
      const newRoom = firebase.database().ref("rooms/").push();
      newRoom.set({ roomname: this.state.nickname });
      alert("register sukses!");
    }
  };

  render() {
    return (
      <>
        <div className="form-container">
          <form className="form" onSubmit={this.handleSubmit} noValidate>
            <h1>
              Get started with us today! Create your account by filling out the
              information below.
            </h1>
            <div className="form-inputs">
              <label className="form-label">Username</label>
              <div className="fullname-input">
                <input
                  className="form-input fullname"
                  type="text"
                  name="nickname"
                  placeholder="username without space"
                  value={this.state.fullname}
                  onChange={this.handleChangeNoSpace}
                  onKeyDown={this.handleKeyDown}
                />
              </div>
              <p>{this.state.nameError}</p>
            </div>
            <div className="form-inputs">
              <label className="form-label">Phone Number</label>
              <input
                className="form-input"
                type="number"
                name="phone"
                placeholder="example: 08xxxxxxxxxx"
                value={this.state.phone}
                onChange={this.handleChange}
              />
              <p>{this.state.phoneError}</p>
            </div>
            <div className="form-inputs">
              <label className="form-label">Email</label>
              <input
                className="form-input"
                type="email"
                name="email"
                placeholder="example: xxx@mail.com"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <p>{this.state.emailError}</p>
            </div>
            <div className="form-inputs">
              <label className="form-label">
                Password (minimal 6 characters)
              </label>
              <input
                className="form-input"
                type="password"
                name="password"
                placeholder="password without space"
                value={this.state.password}
                onChange={this.handleChangeNoSpace}
                onKeyDown={this.onKeyDown}
              />
              <p>{this.state.passwordError}</p>
            </div>
            <div className="form-inputs">
              <label className="form-label">Confirm Password</label>
              <input
                className="form-input"
                type="password"
                name="password2"
                placeholder="Confirm your password"
                value={this.state.password2}
                onChange={this.handleChangeNoSpace}
                onKeyDown={this.handleKeyDown}
              />
              <p>{this.state.password2Error}</p>
            </div>
            <button className="form-input-btn" type="submit">
              Sign Up
            </button>
            <span className="form-input-login">
              Already have an account? Login <Link to="/sign-in">here</Link>
            </span>
          </form>
        </div>
      </>
    );
  }
}
