import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";
import logo from "../../assets/images-user/logo.png";
import { VscAccount } from "react-icons/vsc";
import DropdownUser from "./DropdownUser";

export default function UserNavbar(props) {
  const [click, setClick] = useState(false);
  const [dropdown, setDropdown] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const onMouseEnter = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(true);
    }
  };

  const onMouseLeave = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(false);
    }
  };

  const extendElement = () => {
    dropdown ? setDropdown(false) : setDropdown(true);
  };

  // get user name
  const username = localStorage.getItem("nickname");

  return (
    <nav id="navbar">
      <div id="navbar-container">
        <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
          <img src={logo} alt="logo" className="image-logo"></img>
          BeautySalon
        </Link>
        <div className="menu-icon" onClick={handleClick}>
          <i className={click ? "fas fa-times" : "fas fa-bars"} />
        </div>
        <ul
          className={click ? "nav-menu active" : "nav-menu"}
          style={{
            margin: "0 2rem 0 0",
            padding: "0",
          }}
        >
          <li id="nav-item">
            <Link exact to="/" className="nav-links" onClick={closeMobileMenu}>
              Home
            </Link>
          </li>
          <li id="nav-item">
            <Link
              exact
              to="/services"
              className="nav-links"
              onClick={closeMobileMenu}
            >
              Services
            </Link>
          </li>
          <li id="nav-item">
            <Link
              to="/products"
              className="nav-links"
              onClick={closeMobileMenu}
            >
              Products
            </Link>
          </li>
          <li
            id="nav-item"
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            onClick={extendElement}
          >
            <Link className="nav-links">
              <VscAccount size="30" />
              &nbsp;{username}
            </Link>
            {dropdown && <DropdownUser onCloseMobileMenu={closeMobileMenu} />}
          </li>
        </ul>
      </div>
    </nav>
  );
}
