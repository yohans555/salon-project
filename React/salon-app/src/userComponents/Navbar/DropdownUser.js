import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./Dropdown.css";
import routesUser from "layouts/routesUser";

function DropdownUser(props) {
  const [click, setclick] = useState(false);
  const handleClick = () => setclick(!click);

  // logout tombol
  const history = useHistory();
  const logout = () => {
    localStorage.removeItem("nickname");
    localStorage.removeItem("role");
    history.push("/sign-in");
  };

  return (
    <>
      <ul
        onClick={props.onCloseMobileMenu}
        id={click ? "dropdown-menu clicked" : "dropdown-menu"}
      >
        {routesUser.map((prop, key) => {
          return (
            <li key={key}>
              <a
                className="dropdown-link"
                href={prop.layout + prop.path}
                onClick={() => setclick(false)}
              >
                {prop.name}
              </a>
            </li>
          );
        })}
        <li>
          <a
            className="dropdown-link"
            onClick={(() => setclick(false), logout)}
          >
            Sign Out
          </a>
        </li>
      </ul>
    </>
  );
}

export default DropdownUser;
