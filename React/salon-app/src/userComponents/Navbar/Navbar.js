import React, { useState, useEffect } from "react";
import { Link, NavLink } from "react-router-dom";
import "./Navbar.css";
import logo from "../../assets/images-user/logo.png";

function Navbar(props) {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener("resize", showButton);

  const type = props.type;
  if (type === "signin") {
    return (
      <>
        <nav id="navbar">
          <div id="navbar-container">
            <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
              <img src={logo} alt="logo" className="image-logo"></img>
              BeautySalon
            </Link>
            <div className="menu-icon" onClick={handleClick}>
              <i className={click ? "fas fa-times" : "fas fa-bars"} />
            </div>
            <ul
              className={click ? "nav-menu active" : "nav-menu"}
              style={{ margin: "0 2rem 0 0", padding: "0" }}
            >
              <li id="nav-item">
                <NavLink
                  exact
                  to="/"
                  activeClassName="active-navigation"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Home
                </NavLink>
              </li>
              <li id="nav-item">
                <NavLink
                  exact
                  to="/services"
                  activeClassName="active-navigation"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Services
                </NavLink>
              </li>
              <li id="nav-item">
                <NavLink
                  to="/products"
                  activeClassName="active-navigation"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Products
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </>
    );
  } else {
    return (
      <>
        <nav id="navbar">
          <div id="navbar-container">
            <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
              <img src={logo} alt="logo" className="image-logo"></img>
              BeautySalon
            </Link>
            <div className="menu-icon" onClick={handleClick}>
              <i className={click ? "fas fa-times" : "fas fa-bars"} />
            </div>
            <ul
              className={click ? "nav-menu active" : "nav-menu"}
              style={{ margin: "0 2rem 0 0", padding: "0" }}
            >
              <li id="nav-item">
                <NavLink
                  exact
                  to="/"
                  activeClassName="active-navigation"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Home
                </NavLink>
              </li>
              <li id="nav-item">
                <NavLink
                  exact
                  to="/services"
                  activeClassName="active-navigation"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Services
                </NavLink>
              </li>
              <li id="nav-item">
                <NavLink
                  to="/products"
                  activeClassName="active-navigation"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Products
                </NavLink>
              </li>

              <li>
                <Link
                  to="/sign-in"
                  className="nav-links-mobile"
                  onClick={closeMobileMenu}
                >
                  Sign In
                </Link>
              </li>
            </ul>
            <Link to="/sign-in" className="button-signout">
              SIGN IN
            </Link>
          </div>
        </nav>
      </>
    );
  }
}

export default Navbar;
