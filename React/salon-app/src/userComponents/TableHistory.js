import React, { Component } from "react";
import "./TableHistory.css";
import Service from "../Services/APIServices";
import ConvertToRupiah from "./ConvertToRupiah";

export default class TableHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            historyData: [],
        };
    }

    getUsername = () => {
        const username = localStorage.getItem("nickname");
        return username;
    };

    componentDidMount() {
        Service.getDetail("transaction", this.getUsername()).then((res) => {
            const body = res.data;
            this.setState({ historyData: body });
        });
    }

    getDetails = () => {
        let details = [];
        this.state.historyData.map((data) => {
            let serviceList = [];
            data.services.map((data2) => {
                serviceList.push(data2.name);
            });
            details.push({
                date: data.date,
                stylishFirstName: data.employee.firstName,
                stylishLastName: data.employee.lastName,
                payment: data.status,
                service: serviceList,
                price: ConvertToRupiah(data.total),
            });
        });
        return details;
    };

    render() {
        return (
            <>
                <h3 className='historyTitle'>
                    {this.getUsername()}&apos;s Transaction Details
                </h3>
                {Array.isArray(this.state.historyData) &&
                !this.state.historyData.length ? (
                    <h5 className='message-transaction'>
                        No transaction here. For book reservation{" "}
                        <a href='/user/booking'>click here</a>
                    </h5>
                ) : (
                    <div id='tableContainer'>
                        <table id='userHistory'>
                            <thead>
                                <tr>
                                    <th>Booking Date</th>
                                    <th>Services</th>
                                    <th>Stylish</th>
                                    <th>Payment Status</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.getDetails().map((data, key) => (
                                    <tr key={key}>
                                        <td>{data.date}</td>
                                        <td>{data.service.join(", ")}</td>
                                        <td>
                                            {data.stylishFirstName}{" "}
                                            {data.stylishLastName}
                                        </td>
                                        <td>{data.payment}</td>
                                        <td>{data.price}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                )}
            </>
        );
    }
}
