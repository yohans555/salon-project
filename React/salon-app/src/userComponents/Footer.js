import React from "react";
import "./Footer.css";
import logo from "../assets/images-user/logo.png";
import { Link } from "react-router-dom";
import { MdLocationOn } from "react-icons/md";
import { HiPhone } from "react-icons/hi";
import { FiMail } from "react-icons/fi";
import { FaWhatsapp } from "react-icons/fa";

function Footer() {
  return (
    <div className="footer-container">
      <div className="footer-links">
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>About Us</h2>
            <p>
              BeautySalon is the luxury beauty destination which provides you
              best quality service and a special experience: pampered by
              standarized hairstylists.
            </p>
          </div>
          <div className="footer-link-items">
            <h2>Opening Hours</h2>
            <p>Monday to Sunday, 7 AM - 6 PM</p>
          </div>
          <div className="footer-link-items">
            <h2>Contact Us</h2>
            <p>
              <MdLocationOn size="25" className="icon-address" />
              Scientia Bussiness Park 2
            </p>
            <p>
              <HiPhone size="25" className="icon-address" />
              (021) 7777 7777
            </p>
            <p>
              <FaWhatsapp size="25" className="icon-address" />
              0822 7777 7777
            </p>
            <p>
              <FiMail size="25" className="icon-address" />
              cs@beautysalon.co.id
            </p>
          </div>
        </div>
      </div>
      <section className="social-media">
        <div className="social-media-wrap">
          <div className="footer-logo">
            <Link to="/" className="footer-logo">
              <img src={logo} alt="logo" className="image-logo"></img>
              BeautySalon
            </Link>
          </div>
          <small className="website-rights">BeautySalon © 2021</small>
          <div className="social-icons">
            <a
              className="social-icon-link facebook"
              href="https://id-id.facebook.com/"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-facebook" />
            </a>
            <a
              className="social-icon-link instagram"
              href="https://www.instagram.com/?hl=id"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-instagram" />
            </a>
            <a
              className="social-icon-link youtube"
              href="https://www.youtube.com/"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-youtube" />
            </a>
            <a
              className="social-icon-link twitter"
              href="https://twitter.com/"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-twitter" />
            </a>
            <a
              className="social-icon-link twitter"
              href="https://id.linkedin.com/"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-linkedin" />
            </a>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Footer;
