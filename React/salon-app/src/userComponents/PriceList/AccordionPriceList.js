import React, { Component } from "react";
import styled from "styled-components";
import { IconContext } from "react-icons";
import { FiPlus, FiMinus } from "react-icons/fi";
import Service from "../../Services/APIServices";
import { BsDot } from "react-icons/bs";

const AccordionSection = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background: #fff;
    margin: 2rem 0;
`;

const Container = styled.div`
    box-shadow: 2px 10px 35px 1px rgba(153, 153, 153, 0.3);
    border-radius: 10px;
    background: #272727;
    padding: 6px;
`;

const Wrap = styled.div`
    background: #272727;
    color: #fff;
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 70vw;
    text-align: center;
    cursor: pointer;
    h1 {
        padding: 2rem;
        font-size: 1.5rem;
        margin: 0px;
    }
    span {
        margin-right: 1rem;
    }
`;

const Dropdown = styled.div`
    background: #272727;
    color: #00ffb9;
    width: 100%;
    height: 100px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid #00ffb9;
    border-top: 1px solid #00ffb9;
    padding: 0 2.5rem;
    &:last-child {
        border-bottom: none;
    }
    &:hover {
        background: rgb(26, 23, 23);
        color: #272727;
    }
    p {
        font-size: 1.1rem;
        font-family: "PT Sans", sans-serif;
        font-weight: bold;
        margin: 0;
    }
`;

const Title = styled.div`
    text-align: center;
    font-size: 2rem;
    font-family: "PT Sans", sans-serif;
    font-weight: bold;
    margin-top: 4rem;
`;

export default class AccordionPriceList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
            data: [],
        };
    }

    toggle = (index) => {
        if (this.state.clicked === index) {
            //if clicked menu is already active, then close it
            return this.setState({ clicked: null });
        }
        this.setState({ clicked: index });
    };

    componentDidMount() {
        Service.getData("service").then((res) => {
            const body = res.data;
            this.setState({ data: body });
        });
    }

    convertToRupiah = (angka) => {
        let rupiah = "";
        let angkarev = angka.toString().split("").reverse().join("");
        for (let i = 0; i < angkarev.length; i++)
            if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + ".";
        return (
            "Rp " +
            rupiah
                .split("", rupiah.length - 1)
                .reverse()
                .join("")
        );
    };

    getData = () => {
        let datas = [];
        let service = [];
        let categories = this.state.data.map(
            (item) => item.serviceCategory.name
        );
        let categoryClean = [...new Set(categories)];
        for (let i = 0; i < categoryClean.length; i++) {
            let services = this.state.data.filter(
                (item2) => item2.serviceCategory.name === categoryClean[i]
            );
            service.push(services);
        }
        for (let i = 0; i < service.length; i++) {
            datas.push({
                category: categoryClean[i],
                service: service[i],
            });
        }
        return datas;
    };

    render() {
        return (
            <>
                <Title>
                    Affordable prices with premium services just for you
                </Title>
                <IconContext.Provider value={{ color: "#fff", size: "25px" }}>
                    <AccordionSection>
                        <Container>
                            {this.getData().map((item, index) => {
                                return (
                                    <>
                                        <Wrap
                                            onClick={() => this.toggle(index)}
                                            key={index}
                                        >
                                            <h1>{item.category}</h1>
                                            <span>
                                                {this.state.clicked ===
                                                index ? (
                                                    <FiMinus />
                                                ) : (
                                                    <FiPlus />
                                                )}
                                            </span>
                                        </Wrap>
                                        {this.state.clicked === index ? (
                                            <>
                                                {item.service.map(
                                                    (data, key) => (
                                                        <Dropdown key={key}>
                                                            <p>
                                                                <BsDot />{" "}
                                                                {data.name}
                                                            </p>
                                                            <p>
                                                                {this.convertToRupiah(
                                                                    data.price
                                                                )}
                                                            </p>
                                                        </Dropdown>
                                                    )
                                                )}
                                            </>
                                        ) : null}
                                    </>
                                );
                            })}
                        </Container>
                    </AccordionSection>
                </IconContext.Provider>
            </>
        );
    }
}
