import React from "react";
import { Link } from "react-router-dom";
import "./HeroSection.css";

function HeroSection() {
  return (
    <div className="hero-container">
      <h1>Create Your Style With Us</h1>
      <p>What are you waiting for?</p>
      <div className="hero-btns">
        <Link to="/user/booking" className="btn-booking">
          BOOK NOW
        </Link>
      </div>
    </div>
  );
}

export default HeroSection;
