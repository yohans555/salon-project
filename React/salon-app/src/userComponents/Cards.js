import React from "react";
import "./Cards.css";
import CardItem from "./CardItem";
import img1 from "../assets/images-user/img-1.jpg";
import img2 from "../assets/images-user/img-2.jpg";
import img3 from "../assets/images-user/img-3.jpg";
import img4 from "../assets/images-user/img-4.jpg";
import img5 from "../assets/images-user/img-5.jpg";
import img6 from "../assets/images-user/img-6.jpg";

function Cards(props) {
    return (
        <div className='cards'>
            <h1>Our Premium Treatments and Services</h1>
            <div className='cards__container'>
                <ul className='cards__items'>
                    <CardItem
                        src={img1}
                        text='We will give you the haircut of your dream - bob, layered or simply your regular trim.'
                        label='Hair Cut'
                        path={props.path}
                    />
                    <CardItem
                        src={img2}
                        text='Melt your stress away with a relaxing creambath.'
                        label='Creambath'
                        path={props.path}
                    />
                    <CardItem
                        src={img3}
                        text='Dynamic routines shouldn&apos;t deprive you a special treatment for a perfect look.'
                        label='Washing'
                        path={props.path}
                    />
                    <CardItem
                        src={img4}
                        text='Get your flawless hair color with our beauty experts.'
                        label='Coloring'
                        path={props.path}
                    />
                    <CardItem
                        src={img5}
                        text='We’ll pamper you and leave you with a silky smooth skin.'
                        label='Waxing'
                        path={props.path}
                    />
                    <CardItem
                        src={img6}
                        text='The service to choose when you need to look your best, but not too formal.'
                        label='Styling'
                        path={props.path}
                    />
                </ul>
            </div>
        </div>
    );
}

export default Cards;
