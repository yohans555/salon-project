import React, { Component } from "react";
import "./MyProfileForm.css";
import firebase from "../Firebase";
import { VscAccount } from "react-icons/vsc";

export default class MyProfileForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userProfileList: [],
            userProfile: {},
        };
    }

    handleFormChange = (event) => {
        let copyFormUser = { ...this.state.userProfile };
        copyFormUser[event.target.name] = event.target.value;
        this.setState({ userProfile: copyFormUser });
    };

    handleSubmit = (event) => {
        this.updateData();
        alert("Update success!");
        // event.preventDefault();
    };

    updateData = () => {
        const child = firebase
            .database()
            .ref("users/")
            .child(this.state.userProfile.id);
        child.update({
            email: this.state.userProfile.email,
            nickname: this.state.userProfile.nickname,
            phone: this.state.userProfile.phone,
            password: this.state.userProfile.password,
            password2: this.state.userProfile.password2,
            fullname: this.state.userProfile.fullname,
            address: this.state.userProfile.address,
        });
    };

    componentDidMount() {
        const userRef = firebase.database().ref("users/");
        userRef.on("value", (snapshot) => {
            const users = snapshot.val();
            const userList = [];
            for (let id in users) {
                userList.push({ id, ...users[id] });
            }
            this.setState({ userProfileList: userList });
            this.setState((prevState) => ({
                ...prevState,
                userProfile: this.getUser(),
                isLoaded: false,
            }));
        });
    }

    getUser = () => {
        const userNow = localStorage.getItem("nickname");
        const user = this.state.userProfileList.find(
            (data) => data.nickname == userNow
        );
        return user;
    };

    render() {
        const { userProfile } = this.state;
        return (
            <div className='container-profile'>
                <div className='title'>My Profile</div>
                <div className='user-profile-avatar'>
                    <VscAccount size='80' />
                </div>
                <div className='content'>
                    <form id='profile-form'>
                        <div className='user-details'>
                            <div className='input-box'>
                                <span className='details'>Full Name</span>
                                <input
                                    type='text'
                                    placeholder='Enter your name'
                                    id='input-profile'
                                    name='fullname'
                                    value={userProfile.fullname}
                                    onChange={this.handleFormChange}
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Username</span>
                                <input
                                    type='text'
                                    placeholder='Enter your username'
                                    id='input-profile'
                                    name='nickname'
                                    value={userProfile.nickname}
                                    onChange={this.handleFormChange}
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Email</span>
                                <input
                                    type='text'
                                    placeholder='Enter your email'
                                    id='input-profile'
                                    name='email'
                                    value={userProfile.email}
                                    onChange={this.handleFormChange}
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>Phone Number</span>
                                <input
                                    type='text'
                                    placeholder='Enter your number'
                                    id='input-profile'
                                    name='phone'
                                    value={userProfile.phone}
                                    onChange={this.handleFormChange}
                                />
                            </div>
                            <div className='address-box'>
                                <span className='details'>Address</span>
                                <textarea
                                    name='address'
                                    className='input-address-box'
                                    value={userProfile.address}
                                    onChange={this.handleFormChange}
                                    form='profile-form'
                                ></textarea>
                            </div>
                            <div className='input-box'>
                                <span className='details'>Password</span>
                                <input
                                    type='text'
                                    placeholder='Enter your password'
                                    id='input-profile'
                                    value={userProfile.password}
                                    required
                                    name='password'
                                    onChange={this.handleFormChange}
                                />
                            </div>
                            <div className='input-box'>
                                <span className='details'>
                                    Confirm Password
                                </span>
                                <input
                                    type='text'
                                    placeholder='Confirm your password'
                                    id='input-profile'
                                    name='password2'
                                    value={userProfile.password2}
                                    onChange={this.handleFormChange}
                                    required
                                />
                            </div>
                        </div>
                        <div className='button'>
                            <input
                                type='submit'
                                value='Update'
                                onClick={this.handleSubmit}
                            />
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
