import React, { useState } from "react";
import "./ChatBox.css";
import { IoMdChatboxes } from "react-icons/io";
import Modal from "react-bootstrap/Modal";
import ChatRoom2 from "components/Chat/ChatRoomUser";
import ChatBox2 from "./ChatBox2";

function ChatBox(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const userExist = localStorage.getItem("role");

    return (
        <>
            <button onClick={handleShow} className='chatbox__button'>
                <IoMdChatboxes
                    size='40'
                    color='#fff'
                    style={{ margin: "auto" }}
                />
            </button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton className='chatbox-header'>
                    <Modal.Title className='chatbox-title-box'>
                        <img
                            src='https://image.flaticon.com/icons/png/512/190/190119.png'
                            height='42'
                        ></img>
                        <h3 className='chatbox-title'>BeautySalon</h3>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className='chat-body'>
                    {userExist === "user" ? <ChatRoom2 /> : <ChatBox2 />}
                </Modal.Body>
            </Modal>
        </>
    );
}

export default ChatBox;
