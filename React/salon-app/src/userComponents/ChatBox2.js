import React, { Component } from "react";
import "./ChatBox.css";

export default class ChatBox2 extends Component {
    render() {
        return (
            <>
                <p className='chat-validation'>
                    You must sign in to chat with us!{" "}
                    <a href='/sign-in'>Sign in here</a>
                </p>
            </>
        );
    }
}
