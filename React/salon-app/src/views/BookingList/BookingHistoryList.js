import React from "react";
import TableHistory from "userComponents/TableHistory";

export default function BookingList() {
  return (
    <>
      <h3>Booking History</h3>
      <TableHistory />
    </>
  );
}
