import React from "react";
import TableList from "components/Table/TableList";

export default function BookingList() {
  return (
    <>
      <h3>Booking List Today</h3>
      <TableList />
    </>
  );
}
