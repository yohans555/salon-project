import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useLocation,
} from "react-router-dom";
// import Login from '../../components/Login'
import RoomList from "../../components/Chat/RoomList";
import AddRoom from "../../components/Chat/AddRoom";
import ChatRoom from "../../components/Chat/ChatRoom";
import PropTypes from "prop-types";

function App() {
  let location = useLocation();

  return (
    <Router>
      <div style={{ display: "inline-flex" }}>
        <RoomList />
        <Switch>
          <SecureRoute path="/addroom">
            <AddRoom />
          </SecureRoute>
          <SecureRoute path="/chatroom/:room">
            <ChatRoom />
          </SecureRoute>
        </Switch>
      </div>
    </Router>
  );
}

App.propTypes = {
  children: PropTypes.any.isRequired,
};

export default App;

function SecureRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem("nickname") ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}
