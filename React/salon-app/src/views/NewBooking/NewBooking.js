import moment from "moment";
import React from "react";
import Service from "../../Services/APIServices";
import HandlerService from "../../Services/HandlerServices";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/css/booking.scss";

const dateNow = new Date();
const dateStringTitle = moment(dateNow).format("YYYY-MM-DD");

export default class NewBooking extends React.Component {
  constructor(props) {
    super(props);
    this.total = 0;
    this.state = {
      dataService: [],
      dataEmployee: [],
      service: [{ id: "" }],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.sendts = this.sendts.bind(this);
  }

  async componentDidMount() {
    const responseService = await fetch("http://localhost:8080/get/service");
    const bodyService = await responseService.json();
    this.setState({ dataService: bodyService });

    const responseEmployee = await fetch("http://localhost:8080/get/employee");
    const bodyEmployee = await responseEmployee.json();
    this.setState({ dataEmployee: bodyEmployee });
  }

  sendts() {
    let service = this.state.service;
    let jsondata = {
      username: localStorage.getItem("nickname"),
      total: this.total,
      date: document.getElementById("date").value,
      guestName: document.getElementById("guestName").value,
      phone: document.getElementById("phone").value,
      email: document.getElementById("email").value,
      status: document.getElementById("status").value,
      services: service,
      employee: { id: document.getElementById("employee").value },
    };
    Service.saveData("transaction", jsondata);
    alert("Data booking berhasil!")
    window.location.reload();
  }

  addClick() {
    this.setState((prevState) => ({
      service: [...prevState.service, { id: "" }],
    }));
    let price = HandlerService.getPrice(
      this.state.service,
      this.state.dataService
    );
    this.total = price;
  }

  createUI() {
    return this.state.service.map((el, i) => (
      <div key={i}>
        <select
          id="select-services"
          onChange={this.handleChange2.bind(this, i)}
          name="id"
        >
          <option value="starter" selected>
            --- Choose your services ---
          </option>
          {this.state.dataService.map((item) => (
            <option key={i} value={item.id}>
              {item.serviceCategory.name} : {item.name} &nbsp; (RP. {item.price}
              )
            </option>
          ))}
        </select>
        <input
          type="button"
          id="remove"
          value="—"
          onClick={this.removeClick.bind(this, i)}
        />
      </div>
    ));
  }

  handleChange(i, e) {
    const { name, value } = e.target;
    let service = [...this.state.service];
    service[i] = { ...service[i], [name]: value };
    this.setState({ service });
  }

  handleChange2(i, e) {
    const { name, value } = e.target;
    let service = [...this.state.service];
    service[i] = { ...service[i], [name]: value };
    this.setState({ service });
    let price = HandlerService.getPrice(service, this.state.dataService);
    this.total = price;
  }

  removeClick(i) {
    let service = [...this.state.service];
    service.splice(i, 1);
    this.setState({ service });
    let price = HandlerService.getPrice(service, this.state.dataService);
    this.total = price;
  }

  handleSubmit(event) {
    alert("A name was submitted: " + JSON.stringify(this.state.service));
    event.preventDefault();
  }

  render() {
    const { dataService, dataEmployee } = this.state;
    return (
      <>
        <h3>Booking</h3>
        <div id="form-book">
          <div className="booking-card">
            <div className="title-book">
              <h4>NEW BOOKING</h4>
              <p>{dateStringTitle}</p>
            </div>
            <div className="input-group-guestName">
              <label>Guest Name</label>
              <select>
                <option value="Mr">Mr</option>
                <option value="Mrs">Mrs</option>
              </select>
              <input
                id="guestName"
                name="guestName"
                type="text"
                placeholder="Guest Name"
              />
            </div>
            <div className="input-group-phone">
              <label>Phone</label>
              <input type="number" value="08" className="initPhones" readOnly />
              <label id="labelseparate">-</label>
              <input id="phone" name="phone" type="text" placeholder="Phone" />
            </div>
            <div className="input-group-date">
              <label>Date</label>
              <input
                id="date"
                name="date"
                type="date"
                placeholder="Date"
                min={dateStringTitle}
              />
              <label id="labelemail">Email</label>
              <input id="email" name="email" type="email" placeholder="Email" />
            </div>
            <div className="input-group-services">
              <label>Services</label>
              {this.createUI()}
              <input
                type="button"
                id="buttonAdd"
                value="Add Service"
                onClick={this.addClick.bind(this)}
              />
            </div>
            <div className="input-group-stylish">
              <label>Choose Stylish</label>
              <select id="employee" className="employee">
                {dataEmployee.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.firstName}
                  </option>
                ))}
              </select>
              <label id="status-label">Status</label>
              <select id="status" name="status">
                <option value="Unpaid">Unpaid</option>
                <option value="Paid">Paid</option>
              </select>
            </div>
          </div>
          <div className="send-to-invoice">
            <ul className="list-booking">
              <li>
                <span className="total">Total</span>
                <span className="price">RP. {this.total}</span>
              </li>
              <li>
                <button className="btn-next" onClick={this.sendts}>
                  Send
                </button>
              </li>
            </ul>
          </div>
        </div>
      </>
    );
  }
}
